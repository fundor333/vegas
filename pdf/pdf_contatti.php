<?php
session_start();
require('fpdf.php');
require '../components/function.php';
logged_needed();

class PDFContatti extends FPDF
{
// Load data
    function LoadData($connection)
    {
        if ($_GET['id']) {
            $sql = 'SELECT * FROM Persona WHERE Sottogruppo =' . $_GET['id'] . ' AND (Permessi =1 OR Permessi =2) ORDER BY Nome;';
        } else {
            $sql = 'SELECT * FROM Persona WHERE (Permessi =1 OR Permessi =2) ORDER BY Nome ';
        }
        $data = [];
        foreach ($connection->query($sql) as $persona)
            $data[] = [$persona['Nome'], $persona['Indirizzo'], $persona['Email'], $persona['Telefono'], $persona['Cellulare']];
        return $data;
    }


// Colored table
    function FancyTable($header, $data)
    {
        // Colors, line width and bold font
        $this->SetFillColor(255, 0, 0);
        $this->SetTextColor(255);
        $this->SetDrawColor(128, 0, 0);
        $this->SetLineWidth(.3);
        $this->SetFont('', 'B');
        // Header
        $w = [70, 75, 70, 30, 30];
        for ($i = 0; $i < count($header); $i++)
            $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', true);
        $this->Ln();
        // Color and font restoration
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Data
        $fill = false;
        foreach ($data as $row) {
            $this->Cell($w[0], 6, $row[0], 'LR', 0, 'L', $fill);
            $this->Cell($w[1], 6, $row[1], 'LR', 0, 'L', $fill);
            $this->Cell($w[2], 6, $row[2], 'LR', 0, 'L', $fill);
            $this->Cell($w[3], 6, $row[3], 'LR', 0, 'L', $fill);
            $this->Cell($w[4], 6, $row[4], 'LR', 0, 'L', $fill);
            $this->Ln();
            $fill = !$fill;
        }
        // Closing line
        $this->Cell(array_sum($w), 0, '', 'T');
    }
}

$pdf = new PDFContatti();
// Column headings
$header = ['Nome', 'Indirizzo', 'Email', 'Telefono', 'Cellulare'];
// Data loading
require '../connection.php';
$data = $pdf->LoadData(get_connection());
$pdf->SetFont('Arial', '', 12);
$pdf->AddPage('L');
$pdf->FancyTable($header, $data);
$pdf->Output();
?>