<?php
session_start();
require('fpdf.php');
require '../components/function.php';
logged_needed();

class PDFProduttori extends FPDF
{
// Load data
    function LoadData($connection)
    {
        if ($_GET['id']) {
            $sql = 'SELECT * FROM Produttore WHERE Categoria =' . $_GET['id'] . ' ORDER BY Name;';
        } else {
            $sql = 'SELECT * FROM Produttore ORDER BY Name';
        }
        $data = [];
        foreach ($connection->query($sql) as $persona) {
            $sql = ' SELECT NomeCategoria FROM CategoriaProdotto WHERE Id=' . $persona['Categoria'];
            $name = $connection->query($sql);
            $name = ($name->fetch);
            $data[] = [$name,
                $persona['Prodotto'],
                $persona['Name'],
                $persona['Responsabile'],
                $persona['Dove'],
                $persona['Listino'], $persona['Quando']
            ];
        }
        return $data;
    }


// Colored table
    function FancyTable($data)
    {
        // Colors, line width and bold font
        $this->SetFillColor(255, 0, 0);
        $this->SetTextColor(255);
        $this->SetDrawColor(128, 0, 0);
        $this->SetLineWidth(.3);
        $this->SetFont('', 'B');

        // Color and font restoration
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Data
        $fill = false;
        foreach ($data as $row) {
            $header = ['Prodotto', 'Dettagli', 'Produttore', 'Responabile', 'Consegna', 'Listino', 'Calendario'];

            $this->Cell(40, 6, $header[0], 1, 'L', $fill);
            $this->Cell(0, 6, $row[0], 1, 'L', $fill);
            $this->Ln();

            $this->Cell(40, 6, $header[1], 1, 'L', $fill);
            $this->Cell(0, 6, $row[1], 1, 'L', $fill);
            $this->Ln();

            $this->Cell(40, 6, $header[2], 1, 'L', $fill);
            $this->Cell(0, 6, $row[2], 1, 'L', $fill);
            $this->Ln();

            $this->Cell(40, 6, $header[3], 1, 'L', $fill);
            $this->Cell(0, 6, $row[3], 1, 'L', $fill);
            $this->Ln();

            $this->Cell(40, 6, $header[4], 1, 'L', $fill);
            $this->Cell(0, 6, $row[4], 1, 'L', $fill);
            $this->Ln();

            $this->Cell(40, 6, $header[5], 1, 'L', $fill);
            $this->Cell(0, 6, $row[5], 1, 'L', $fill);
            $this->Ln();

            $this->Cell(40, 6, $header[6], 1, 'L', $fill);
            $this->Cell(0, 6, $row[6], 1, 'L', $fill);
            $this->Ln();

            $fill = !$fill;
        }
        // Closing line
        $this->Cell(0, 0, '', 'T');
    }
}

$pdf = new PDFProduttori();
// Column headings
// Data loading
require '../connection.php';
$data = $pdf->LoadData(get_connection());
$pdf->SetFont('Arial', '', 8);
$pdf->AddPage('L');
$pdf->FancyTable($data);
$pdf->Output();