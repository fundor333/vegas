<?php
session_start();
require 'components/function.php';
go_logged();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : pagina ufficiale : Benvenuti!</title>
    <link href="css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="css/form_styles.css" rel="stylesheet" type="text/css"/>

    <link rel="apple-touch-icon" sizes="57x57" href="imgages/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="imgages/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="imgages/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="imgages/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="imgages/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="imgages/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="imgages/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="imgages/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="imgages/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="imgages/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="imgages/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="imgages/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="imgages/favicon-16x16.png">
    <link rel="manifest" href="imgages/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="imgages/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

</head>

<body>

<div id="wrapper">

    <?php require "components/header.php"; ?>

    <div id="content">
        <?php require "components/left_content.php"; ?>
        <div id="right_content">
            <div id="title">HOME : BENVENUTO</div><!-- end title div -->
            <div id="article">Vi diamo il benvenuto sul sito del Veneziano GAS! <br/> Il Veneziano Gas e presente nel
                isola veneziana dal 2001. Qui potete trovare tutte le informazioni necessarie e scoprire cosa è un Gas,
                quali sono il suoi valori e le modalità di iscrizione. I gassisti già registrati hanno inoltre la
                possibilità di accedere all'area riservata dove visualizare informazioni, forum, verbali e altri
                documenti riservati ai soli iscritti. <br/>
                Buona continuazione!<br/><br/>
                <img src="images/Principi_GAS.png" width="730" height="562" alt="Principi Veneziano Gas"
                     longdesc="http://http://www.venezianogas.net/"/><br/> I G.A.S., il consumo critico.<br/>
                I Gruppi di Acquisto Solidale – G.A.S. – attivi in Italia da circa quindici anni, nascono dall’esigenza
                di riflettere sul proprio modo di consumare, spesso poco attento, e di realizzare una valida alternativa
                acquistando prodotti di uso comune seguendo, quali criteri guida, i concetti di salute, solidarietà,
                sostenibilità ambientale e giustizia sociale.<br/><br/>
                Nella pratica i Gas promuovono:<br/>
                - l’acquisto di prodotti etici e biologici, possibilmente da piccoli produttori locali;<br/>
                - l’informazione e la sensibilizzazione su questi temi;<br/>
                - lo sviluppo di contatti diretti e di iniziative comuni tra produttori e consumatori.<br/>
                I GAS non cercano, quindi, solo di risparmiare acquistando in gruppo, ma soprattutto di creare mentalità
                e comportamenti di consumo responsabili e consapevoli.<br/><br/></div><!-- end article div -->
        </div><!-- end right_content div -->
    </div><!-- end content div -->

    <?php require "components/footer.php"; ?>
</div>
<!-- end wrapper div -->


</body>
</html>
