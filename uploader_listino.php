<?php
if (isset($_POST['action']) and $_POST['action'] == 'upload') {
    require 'connection.php';
    $todaydate = time();
    $connection = get_connection();
    $directory_path = "/uploads/Listini/";
    $sql = "SELECT * FROM Produttore WHERE Id=" . $_POST['produttore'];
    $name = "";
    foreach ($connection->query($sql) as $group) {
        $name = $group['Id'];
    }
    $name = $name . $todaydate . '.pdf';
    define("UPLOAD_DIR", '.' . $directory_path);
    $sql = "INSERT INTO `Documenti` (`Url`, `Name`, `Data`) VALUES ( '" . $directory_path . $name . "', '" . $name . "', '" . $todaydate . "')";
    $flag = false;
    if (isset($_FILES['user_file'])) {
        $file = $_FILES['user_file'];
        if ($file['error'] == UPLOAD_ERR_OK and is_uploaded_file($file['tmp_name'])) {
            move_uploaded_file($file['tmp_name'], UPLOAD_DIR . $name);

            $connection->query($sql);
            $sql = 'SELECT * FROM Documenti WHERE Name ="' . $name . '"';
            foreach ($connection->query($sql) as $s) {
                $sql = 'UPDATE `Produttore` SET Listino=' . $s['Id'] . ' WHERE Id=' . $_POST['produttore'];

            }
            $connection->query($sql);
            $flag = true;

        }
    }
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : pagina ufficiale : Link verso siti esterni</title>
    <link href="css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>
<body>

<div id="wrapper">
    <?php require "components/header.php" ?>

    <div id="content">
        <?php require "components/left_content.php" ?>

        <div id="right_content">
            <div id="title">LINK</div><!-- end title div -->
            <div id="article">
                <?php
                if ($flag) {
                    echo "Hai caricato correttamente il file. Ora è disponibile nella pagina <a href='pages/documenti.php'>Documenti</a>";
                } else {
                    echo "Hai riscontato problemi con l'upload";
                }
                ?>

            </div><!-- end article div -->


        </div><!-- end right_content div -->
    </div><!-- end content div -->

    <?php require "components/footer.php" ?>
</div>
<!-- end wrapper div -->


</body>
</html>
