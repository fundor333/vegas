<?php session_start(); ?>
<?php require 'components/function.php';
require 'connection.php';
$connection=get_connection();

if (isset($_POST['LogIn'])) {
    $Email = $_POST['Email'];
    $PW = $_POST['Password'];
    checklogin($Email, $PW, get_connection());
    $_SESSION['admin'] = checkadmin($_SESSION['type_user'], get_connection());
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : pagina ufficiale : Benvenuti!</title>
    <link href="css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php
    require 'components/header.php';
    ?>
    <div id="content">
        <?php require "components/left_content.php"; ?>

        <div id="right_content">
            <div id="title">HAI DIMENTICATO LA PASSWORD?</div>
            <div id="article">
                <?php
                if (isset($_POST['invia'])) {

                    $errore = 0; //variabile di controllo errori (se rimane a 0 non ci sono errori)

                    if ($_POST['email'] == "") {
                        $errore = 1;
                        echo "<p>Email vuota</p>";
                    } else {
                        $sql = "SELECT * FROM Persona WHERE Email ='" . $_POST['email']."';" ;
                        $user = null;
                        foreach ($connection->query($sql) as $row) {
                            $user = $row;
                        }
                    }
                    if ($user) {
                        $hash = $user['Password'];
                    } else
                    {
                        echo "<p>Non ho trovato l'utente</p>";
                    $errore = 1;
                    }
                    //se non ci sono stati errori, invio l’email all’utente con il link da confermare
                    if ($errore == 0) {

                        $header = "From: sito.it <sito@venezianogas.net>\n";
                        $header .= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
                        $header .= "Content-Transfer-Encoding: 7bit\n\n";

                        $subject = "VenezianoGas - Password dimenticata";

                        $mess_invio = "<html><body>";
                        $mess_invio .= "E' stata fatta una rihciesta per il cambio password dell'account legato a questa mail. Clicca sul <a href=\"http://www.venezianogas.net/pages/recupero_password.php?hash=" . $hash . "\">link</a> per confermare la richiesta e ricevere la nuova password.<br /> Se il link non è visibile, copia la riga qui sotto e incollala sul tuo browser: <br /> http://www.venezianogas.net/pages/recupero_password.php?hash=" . $hash . " ";
                        $mess_invio .= '</body><html>';

                        //invio email
                        if (mail($_POST['email'], $subject, $mess_invio, $header)) {

                            echo "<div class=\"campo_contatti\" style=\"margin-left: 20px; height: 300px\">";
                            echo "Email inviata con successo. Controlla la tua email<br /><br />";

                            echo "</div> <div class=\"clear\"></div>";
                            unset($_POST); //elimino le variabili post, in modo che non appaiano nel form

                        }
                        else{
                            echo "<h1>Problemi di invio della posta</h1>";
                        }

                    }
                }

                ?>
                <form action="" method="post" id="login">

                    <div class="campo_contatti">
                        <div class="voce_campo">Inserisci la tua email per ricevere la nuova password</div>
                        <input type="text" name="email" value="email@qualcosa.com" class="campo"/>
                    </div>

                    <div class="clear"></div>

                    <div class="campo_contatti">
                        <input type="submit" value="invia" name="invia"/>
                    </div>

                    <div class="clear"></div>

                </form>
            </div>
            <div id="title">NON TI SEI ANCORA REGISTRATO</div><!-- end title div -->
            <div id="article">
                <p>Caro utente non ti sei ancora registrato o non puoi accedere all'area riservata.</p>
                <p>&nbsp;</p>
                <p>Prova a registrarti di nuovo o completa l'iscrizione.<br/>
                </p>
            </div>
            <!-- end article div -->
        </div><!-- end right_content div -->
    </div><!-- end content div -->

    <?php require 'components/footer.php'; ?> <!-- end footer div -->
</div>
<!-- end wrapper div -->


</body>
</html>
