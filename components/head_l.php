<div id="nmenu">
    <div id="navMenu_logIn">
        <ul>
            <li>
                <a href=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . "/pages/benvenuto.php" ?>>HOME</a>
            </li>
        </ul>
        <ul>
            <li>
                <a href=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . "/pages/ruoli.php" ?>>RUOLI</a>
                <ul>
                    <li>
                        <a href=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . "/pages/prodotti.php" ?>>PRODOTTI</a>
                    </li><!-- end inner LI-->
                </ul><!-- end inner UL -->
            </li><!-- end main LI -->
        </ul><!-- end main UL -->

        <ul>
            <li><a href=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . "/pages/documenti.php" ?>>DOCUMENTI</a></li>
            <!-- end main LI -->
        </ul><!-- end main UL -->

        <ul>
            <li><a href=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . "/pages/istruzioni.php" ?>>ISTRUZIONI</a>

            </li><!-- end main LI -->
        </ul><!-- end main UL -->
        <ul>
            <li><a href=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . "/pages/contattiGAS.php" ?>>CONTATTI</a>

            </li><!-- end main LI -->
        </ul><!-- end main UL -->
        <?php
        require_once '../components/function.php';
        require_once '../connection.php';
        if (checkadmin($_SESSION['type_user'], get_connection())) {
            echo '<ul><li><a href=' . 'http://' . $_SERVER['HTTP_HOST'] . "/admin/index.php" . '>Admin</a></li></ul>';
        }
        ?>
        <br class="clearFloat"/>
    </div> <!-- end navMenu div -->
</div><!-- end nmenu div -->
