<?php
require_once '../connection.php';


function mail_list($command, $from, $mail_list)
{
    $from = str_replace('@', '=', strip_tags($from));
    $adress = $mail_list . '-' . $command . '-' . $from . '@venezianogas.net';
    echo mail($adress, '', '');
}

function subscribe($from, $name_mail_list)
{
    mail_list('subscribe', $from, $name_mail_list);
}

function unsubscribe($from, $mail_list)
{
    mail_list('unsubscribe', $from, $mail_list);
}

function help($from, $subject, $txt)
{
    $to = "tecnico@venezianogas.net";
    $headers = "From: " . $from . "\r\n";
    mail($to, $subject, $txt, $headers);
}

function mailist_from_group($group_id)
{
    $con = get_connection();
    $sql = "SELECT Nome FROM MailList  WHERE Id = (SELECT MailList FROM Gruppi WHERE Id = $group_id)";
    foreach ($con->query($sql) as $e) {
        echo $e;
        return $e;
    }
    return null;
}

function send_new_password($email,$password)
{
    $manager_pw = new PasswordHash(8, TRUE);
    $password_new = $manager_pw->HashPassword($password);
    

    
    $header= "From: venezianogas.net <sito@venezianogas.net>\n";
    $header .= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
    $header .= "Content-Transfer-Encoding: 7bit\n\n";
    
    $subject= "Venezianogas.net - Nuova password utente";
    
    $mess_invio="<html><body>";
    $mess_invio.=" La sua nuova password utente è <br/>".$password."<br /> Ora puoi accedere all'area riservata <a href=\"http://www.venezianogas.net\" style=\"color: red\">Sito</a>. ";
    $mess_invio.='</body><html>';
    
    if(mail($email, $subject, $mess_invio, $header)) {
        ?>
                                La password è stata cambiata con successo. Controlla la tua email.<br/><br/>
                                <?php
                            }
                            else{
                                ?>
                                Il link non valido. <br/><br/>
                                <?php
                            }
}

?>
