<!-- TODO Fix the connection -->
<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/components/function.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/connection.php';

if (isset($_POST['LogIn'])) {
    $Email = $_POST['Email'];
    $PW = $_POST['Password'];
    checklogin($Email, $PW, get_connection());
}
?>

<div id="left_content">
    <div id="title">LOGIN ISCRITTI</div><!-- end title div -->
    <div id="article">
        <div id="login_form">
            <form action="../login.php" method="post" id="LogIn">

                <div id="formTXfield">
                    <input name="Email" type="email" required placeholder="EMAIL"/></div>
                <!-- end formTXfield div -->

                <div id="formTXfield">
                    <input name="Password" type="password" required placeholder="PASSWORD"/></div>
                <!-- end formTXfield div -->

                <div id="formButton">
                    <input name="LogIn" type="submit" value="ENTRA" id="LogIn"/></div>
                <!-- end formButton div -->
            </form>

        </div><!-- end login_form div -->
    </div><!-- end article div -->
    <div id="title">Password dimenticata</div><!-- end title div -->
    <div id="article">
        <div id="login_form">
           <p>Password dimenticata? Clicca <a href=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . "/login.php" ?>>qui</a></p>

        </div><!-- end login_form div -->
    </div><!-- end article div -->
    <div id="title">REGISTRAZIONE</div><!-- end title div -->
    <div id="article_yellow">
        <a href=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . "/pages/come_iscriversi.php" ?>>Scopri come iscriverti al
            Veneziano GAS, registrarti al sito e
            scarica il kit di benvenuto dedicato ai nuovi gassisti.</a></div><!-- end article div -->
    <div id="title">PRODUTTORI</div><!-- end title div -->
    <div id="article_orange">
        <a href=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . "/pages/produttori.php" ?>>Sei un produttore e vuoi
            proporre i tuoi prodotti al Veneziano GAS. Qui
            troverai tutte le informazioni necessarie. </a></div><!-- end article div -->
    <div id="title">ORDINI</div><!-- end title div -->
    <div id="article_blue">
        <a href="https://www.eventhia.com/" target="_new">Entra nel
            sito di Economia Solidale per fare i tuoi
            ordini. </a></div><!-- end article div -->
    <!--
    <div id="title">?##?</div>
    <div id="article_purple">
        <a href=<?php /*echo 'http://' . $_SERVER['HTTP_HOST'] . "#" */ ?> target="_new">jkhvbjkbjhlbjkljlkjlbjlbh</a>
    </div>
-->

</div><!-- end left_content div -->
