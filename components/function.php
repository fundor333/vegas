<?php

function logged_needed()
{
    if ($_SESSION['logged'] == false) {
        header('Location: ' . "http://$_SERVER[HTTP_HOST]" . '/index.php');
    }
}

function go_logged()
{
    if ($_SESSION['logged'] == true) {
        header('Location: ' . "http://$_SERVER[HTTP_HOST]" . '/pages/benvenuto.php');
    }
}

function admin_needed()
{
    if ($_SESSION['admin'] == false) {
        header('Location: ' . "http://$_SERVER[HTTP_HOST]" . '/pages/benvenuto.php');
    }
}

function is_logged()
{
    return $_SESSION['logged'] == "true";
}

function checkadmin($permessi, $connection)
{
    $sql = 'SELECT Label FROM Permessi WHERE Id=' . $permessi;
    $flag = false;
    $result =$connection->query($sql);
    if ($result)  {
        foreach ($result as $row) {
            $flag = $row['Label'] == 'Amministratore';
        }
        return $flag;
    }
    return false;

}

function checklogin($login, $password, $connection)
{
    $sql = "SELECT * FROM Persona WHERE Email='$login'";
    $user = null;
    foreach ($connection->query($sql) as $row) {
        $user = $row;
    }
    $check = checkpassword($password, $user['Password']);
    if ($check) {
        $_SESSION['logged'] = true;
        $_SESSION['timeout'] = time();
        $_SESSION['username'] = $login;
        $_SESSION['nome'] = $user['Nome'];
        $_SESSION['type_user'] = $user['Permessi'];
        $referente = null;
        $sql = "SELECT * FROM Referente WHERE Persona=" . $user['Id'];
        foreach ($connection->query($sql) as $row) {
            $referente[] = $row;
        }
        $_SESSION['referente'] = $referente;

        header('Location: ' . "http://$_SERVER[HTTP_HOST]" . '/pages/benvenuto.php');
        return 'Benvenuto';
    } else {
        return 'Password o login sbagliato';
    }
}

function logout()
{
    unset($_SESSION["logged"]);
    session_destroy();
    session_start();
}

function checkpassword($password, $hash)
{
    require_once "class-phpass.php";
    // By default, use the portable hash from phpass
    $wp_hasher = new PasswordHash(8, TRUE);

    $check = $wp_hasher->CheckPassword($password, $hash);
    if ($check == null) {
        return false;
    }
    return $check;
}

function random($lunghezza=12){
    $caratteri_disponibili ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    $codice = "";
    for($i = 0; $i<$lunghezza; $i++){
        $codice = $codice.substr($caratteri_disponibili,rand(0,strlen($caratteri_disponibili)-1),1);
    }
    return $codice;
}

?>