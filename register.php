<?php require 'components/function.php'; ?>
<?php
if (isset($_POST['Register'])) {
    session_start();
    $FName = $_POST['Fname'];
    $LName = $_POST['Lname'];
    $User = $_POST['User'];
    $Email = $_POST['Email'];
    $PW = $_POST['Password'];
    $StorePassword = password_hash($Pw, PASSWORD_BCRYPT, array('cost' => 10));
    $sql = getconnection()->query("INSERT INTO $wpdb->users (Fname, Lname, User, Email, Password)Values('{$FName}', '{$LName}', '{$User}', '{$Email}', '{$StorePassword}')");
    header('Location: login.php');
}
?>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VE G.A.S. - MODULO DI REGISTRAZIONE NUOVO UTENTE</title>
    <title>VENEZIANO GAS : pagina ufficiale : Benvenuti!</title>
    <link href="css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php require "components/header.php"; ?>
    <div id="content">
        <?php require "components/left_content.php"; ?>

        <div id="right_content">
            <div id="title">HOME : BENVENUTO</div><!-- end title div -->
            <div id="article">
                <form action="" method="post" id="register_form">

                    <div id="formTXfield">
                        <input name="Fname" type="text" required placeholder="NOME"/></div><!-- end formTXfield div -->
                    <div id="formTXfield">
                        <input name="Lname" type="text" required placeholder="COGNOME"/></div>
                    <!-- end formTXfield div -->
                    <div id="formTXfield">
                        <input name="Email" type="email" required placeholder="EMAIL"/></div>
                    <!-- end formTXfield div -->
                    <div id="formTXfield">
                        <input name="User" type="text" required placeholder="NOME UTENTE"/></div>
                    <!-- end formTXfield div -->

                    <div id="formTXfield">
                        <input name="Cell" type="text" required placeholder="CELLULARE"/></div>
                    <!-- end formTXfield div -->
                    <div id="formTXfield">
                        <input name="Password" type="password" required placeholder="PASSWORD"/></div>
                    <!-- end formTXfield div -->
                    <div id="formTXfield">
                        <select name="Gruppo2" size="1" required id="Gruppo">
                            <option value="000" selected>SOTTOGRUPPO</option>
                            <option value="Cannareggio Alto">Cannareggio Alto</option>
                            <option value="Cannaregio Basso">Cannaregio Basso</option>
                            <option value="Castello San Marco">Castello / San Marco</option>
                            <option value="Giudecca">Giudecca</option>
                            <option value="S Croce Polo Dorsoduro">S. Croce / Dorsoduro / S. Polo</option>
                            <option value="Lido">Lido</option>
                        </select>
                    </div><!-- end formTXfield div -->
                    <div id="formButton"><input name="Register" type="submit" id="INVIA" value="INVIA"/></div>
                    <!-- end formButton div -->
                </form>
                <br/><br/>
            </div><!-- end article div -->
        </div><!-- end right_content div -->
    </div><!-- end content div -->

    <?php require "components/footer.php"; ?>
</div>
<!-- end wrapper div -->


</body>
</html>
