CREATE TABLE Produttore
(
  Id          INT NOT NULL AUTO_INCREMENT,
  Name        TEXT,
  Prodotto    TEXT,
  Listino     INT,
  Dove        TEXT,
  Quando      TEXT,
  Certificato TEXT,
  Modalita    TEXT,
  Sito        TEXT,
  Categoria   INT,
  Immagine    TEXT,

  PRIMARY KEY (Id)
);

CREATE TABLE CategoriaProdotto
(
  NomeCategoria TEXT,
  ID            INT NOT NULL AUTO_INCREMENT,

  PRIMARY KEY (ID)
);

CREATE TABLE Persona
(
  Id          INT NOT NULL AUTO_INCREMENT,
  Password    TEXT,
  Indirizzo   TEXT,
  Email       TEXT,
  Permessi    INT,
  Telefono    TEXT,
  Cellulare   TEXT,
  Nome        TEXT,
  Sottogruppo INT,

  PRIMARY KEY (Id)
);

CREATE TABLE Link
(
  Id    INT NOT NULL AUTO_INCREMENT,
  Testo TEXT,
  Image TEXT,
  Site  TEXT,

  PRIMARY KEY (Id)
);

CREATE TABLE Documenti
(
  Id   INT NOT NULL AUTO_INCREMENT,
  Url  TEXT,
  Name TEXT,
  Data DATE,

  PRIMARY KEY (Id)
);

CREATE TABLE Referente
(
  Persona    INT,
  Produttore INT,

  PRIMARY KEY (Persona, Produttore),
  FOREIGN KEY (Persona) REFERENCES Persona (Id),
  FOREIGN KEY (Produttore) REFERENCES Produttore (Id)
);

CREATE TABLE Permessi
(
  Id    INT NOT NULL AUTO_INCREMENT,
  Label TEXT,

  PRIMARY KEY (Id)
);

CREATE TABLE Gruppi
(
  Name     VARCHAR(64) UNIQUE NOT NULL,
  Id       INT AUTO_INCREMENT,
  MailList INT,

  PRIMARY KEY (Id)
);

CREATE TABLE DocumentoXGruppo
(
  Documento INT,
  Gruppo    INT,

  PRIMARY KEY (Documento, Gruppo)
);

CREATE TABLE CapoGruppo
(
  Gruppo  INT,
  Persona INT,

  PRIMARY KEY (Gruppo, Persona)
);

CREATE TABLE Ruoli
(
  Nome    TEXT,
  Id      INT NOT NULL AUTO_INCREMENT,
  Persona INT,
  PRIMARY KEY (Id)
);

CREATE TABLE MailList
(
  Id        INT AUTO_INCREMENT NOT NULL UNIQUE,
  Nome      VARCHAR(80) UNIQUE,
  Scopo     TEXT,
  Indirizzo TEXT,
  PRIMARY KEY (Id)
);

CREATE TABLE ConfigurazioniEPassword
(
  Id          INT AUTO_INCREMENT,
  Testo       TEXT,
  Password    TEXT,
  Spiegazione TEXT,
  PRIMARY KEY (Id)

);

CREATE TABLE MeseXProduttore
(
  Mese       INT,
  Produttore INT,
  PRIMARY KEY (Mese, Produttore),
  FOREIGN KEY (Mese) REFERENCES Mese (Id),
  FOREIGN KEY (Produttore) REFERENCES Produttore (Id)
);

ALTER TABLE Gruppi
  ADD FOREIGN KEY (MailList)
REFERENCES MailList (Id);

ALTER TABLE Produttore
  ADD FOREIGN KEY (Categoria)
REFERENCES CategoriaProdotto (Id);

ALTER TABLE Produttore
  ADD FOREIGN KEY (Listino)
REFERENCES Documenti (Id);


ALTER TABLE Persona
  ADD FOREIGN KEY (Permessi)
REFERENCES Permessi (Id);

ALTER TABLE Persona
  ADD FOREIGN KEY (Sottogruppo)
REFERENCES Gruppi (Id);

ALTER TABLE DocumentoXGruppo
  ADD FOREIGN KEY (Documento) REFERENCES Documenti (Id),
  ADD FOREIGN KEY (Gruppo) REFERENCES Gruppi (Id);

ALTER TABLE Ruoli
  ADD FOREIGN KEY (Persona)
REFERENCES Persona (Id);