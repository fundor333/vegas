-- phpMyAdmin SQL Dump
-- version 3.4.7.1
-- http://www.phpmyadmin.net
--
-- Host: 62.149.150.114
-- Generato il: Nov 11, 2016 alle 09:55
-- Versione del server: 5.0.92
-- Versione PHP: 5.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Sql341301_3`
--


-- --------------------------------------------------------

--
-- Struttura della tabella `CapoGruppo`
--

CREATE TABLE IF NOT EXISTS `CapoGruppo` (
  `Gruppo`  INT(11) NOT NULL DEFAULT '0',
  `Persona` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Gruppo`, `Persona`)
)
  ENGINE = MyISAM
  DEFAULT CHARSET = latin1;

--
-- Dump dei dati per la tabella `CapoGruppo`
--

INSERT INTO `CapoGruppo` (`Gruppo`, `Persona`) VALUES
  (3, 12),
  (5, 15),
  (5, 144),
  (6, 7),
  (6, 32),
  (6, 106),
  (7, 105),
  (8, 2),
  (8, 40),
  (8, 58),
  (8, 106),
  (8, 182),
  (8, 188),
  (10, 42),
  (10, 53),
  (10, 67);

-- --------------------------------------------------------

--
-- Struttura della tabella `CategoriaProdotto`
--

CREATE TABLE IF NOT EXISTS `CategoriaProdotto` (
  `NomeCategoria` TEXT,
  `ID`            INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`)
)
  ENGINE = MyISAM
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 11;

--
-- Dump dei dati per la tabella `CategoriaProdotto`
--

INSERT INTO `CategoriaProdotto` (`NomeCategoria`, `ID`) VALUES
  ('Sconosciuto', 1),
  ('FRUTTA E VERDURA', 2),
  ('PANE, FARINA E BISCOTTI', 3),
  ('PASTA E RISO', 4),
  ('MIELE, CAFFE E SPEZIE', 5),
  ('OLIO, SOTT''OLIO, VINO E ACETO', 6),
  ('CARNE E PESCE', 7),
  ('FORMAGGI', 8),
  ('DETERSIVI, COSMESI e OLII ESSENZIALI', 9),
  ('TESSILI E CALZATURE', 10);

-- --------------------------------------------------------

--
-- Struttura della tabella `ConfigurazioniEPassword`
--

CREATE TABLE IF NOT EXISTS `ConfigurazioniEPassword` (
  `Id`          INT(11) NOT NULL AUTO_INCREMENT,
  `Testo`       TEXT,
  `Password`    TEXT,
  `Spiegazione` TEXT,
  PRIMARY KEY (`Id`)
)
  ENGINE = MyISAM
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 3;

--
-- Dump dei dati per la tabella `ConfigurazioniEPassword`
--

INSERT INTO `ConfigurazioniEPassword` (`Id`, `Testo`, `Password`, `Spiegazione`) VALUES
  (1, 'sito@venezianogas.net', 'ombrellone16', 'Email e password per il sito'),
  (2, 'postmaster@venezianogas.net', 'presente2015!', 'Password per la gestione delle mail list e account');

-- --------------------------------------------------------

--
-- Struttura della tabella `Documenti`
--

CREATE TABLE IF NOT EXISTS `Documenti` (
  `Id`   INT(11) NOT NULL AUTO_INCREMENT,
  `Url`  TEXT,
  `Name` TEXT,
  `Data` DATE             DEFAULT NULL,
  PRIMARY KEY (`Id`)
)
  ENGINE = MyISAM
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 28;

--
-- Dump dei dati per la tabella `Documenti`
--

INSERT INTO `Documenti` (`Id`, `Url`, `Name`, `Data`) VALUES
  (1, '/uploads/1/San Polo161019.pdf', 'San Polo161019.pdf', '2016-10-19'),
  (2, '/uploads/6/Cannareggio Basso16-06-27.pdf', 'Cannareggio Basso16-06-27.pdf', '2016-06-27'),
  (3, '/uploads/6/Cannareggio Basso2016-10-27.pdf', 'Cannareggio Basso2016-10-27.pdf', '2016-10-27'),
  (4, '/uploads//CampoverdeEurope/Rome.pdf', 'CampoverdeEurope/Rome.pdf', '0000-00-00'),
  (5, '/uploads//CampoverdeEurope/Rome.pdf', 'CampoverdeEurope/Rome.pdf', '0000-00-00'),
  (6, '/uploads/Listini/CampoverdeEurope/Rome.pdf', 'CampoverdeEurope/Rome.pdf', '0000-00-00'),
  (7, '/uploads/Listini/15Europe/Rome.pdf', '15Europe/Rome.pdf', '0000-00-00'),
  (8, '/uploads/Listini/151478086949.pdf', '151478086949.pdf', '0000-00-00'),
  (9, '/uploads/Listini/201478089387.pdf', '201478089387.pdf', '0000-00-00'),
  (10, '/uploads/Listini/171478089431.pdf', '171478089431.pdf', '0000-00-00'),
  (11, '/uploads/Listini/61478090063.pdf', '61478090063.pdf', '0000-00-00'),
  (12, '/uploads/Listini/51478090121.pdf', '51478090121.pdf', '0000-00-00'),
  (13, '/uploads/Listini/171478090164.pdf', '171478090164.pdf', '0000-00-00'),
  (14, '/uploads/Listini/221478090281.pdf', '221478090281.pdf', '0000-00-00'),
  (15, '/uploads/Listini/41478090341.pdf', '41478090341.pdf', '0000-00-00'),
  (16, '/uploads/Listini/121478090362.pdf', '121478090362.pdf', '0000-00-00'),
  (17, '/uploads/Listini/131478090421.pdf', '131478090421.pdf', '0000-00-00'),
  (18, '/uploads/Listini/31478090446.pdf', '31478090446.pdf', '0000-00-00'),
  (19, '/uploads/Listini/81478090469.pdf', '81478090469.pdf', '0000-00-00'),
  (20, '/uploads/Listini/91478090763.pdf', '91478090763.pdf', '0000-00-00'),
  (21, '/uploads/Listini/141478090805.pdf', '141478090805.pdf', '0000-00-00'),
  (22, '/uploads/Listini/231478090828.pdf', '231478090828.pdf', '0000-00-00'),
  (23, '/uploads/Listini/161478090888.pdf', '161478090888.pdf', '0000-00-00'),
  (24, '/uploads/Listini/11478090944.pdf', '11478090944.pdf', '0000-00-00'),
  (25, '/uploads/Listini/261478090962.pdf', '261478090962.pdf', '0000-00-00'),
  (26, '/uploads/Listini/261478091008.pdf', '261478091008.pdf', '0000-00-00'),
  (27, '/uploads/5/Cannareggio Alto2016-10-17.pdf', 'Cannareggio Alto2016-10-17.pdf', '2016-10-17');

-- --------------------------------------------------------

--
-- Struttura della tabella `DocumentoXGruppo`
--

CREATE TABLE IF NOT EXISTS `DocumentoXGruppo` (
  `Documento` INT(11) NOT NULL DEFAULT '0',
  `Gruppo`    INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Documento`, `Gruppo`),
  KEY `Gruppo` (`Gruppo`)
)
  ENGINE = MyISAM
  DEFAULT CHARSET = latin1;

--
-- Dump dei dati per la tabella `DocumentoXGruppo`
--

INSERT INTO `DocumentoXGruppo` (`Documento`, `Gruppo`) VALUES
  (1, 1),
  (2, 6),
  (3, 6),
  (27, 5);

-- --------------------------------------------------------

--
-- Struttura della tabella `Gruppi`
--

CREATE TABLE IF NOT EXISTS `Gruppi` (
  `Name`     VARCHAR(64) NOT NULL,
  `Id`       INT(11)     NOT NULL AUTO_INCREMENT,
  `MailList` INT(11)              DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Name` (`Name`),
  KEY `MailList` (`MailList`)
)
  ENGINE = MyISAM
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 11;

--
-- Dump dei dati per la tabella `Gruppi`
--

INSERT INTO `Gruppi` (`Name`, `Id`, `MailList`) VALUES
  ('San Polo', 1, 6),
  ('San Marco', 2, 5),
  ('Lido', 3, 4),
  ('Giudecca', 4, 7),
  ('Cannareggio Alto', 5, 1),
  ('Cannareggio Basso', 6, 2),
  ('Plenaria', 7, 11),
  ('Accoglienza', 8, 8),
  ('Prodotti', 9, 9),
  ('Sintesi', 10, 10);

-- --------------------------------------------------------

--
-- Struttura della tabella `Link`
--

CREATE TABLE IF NOT EXISTS `Link` (
  `Id`    INT(11) NOT NULL AUTO_INCREMENT,
  `Testo` TEXT,
  `Image` TEXT,
  `Site`  TEXT,
  PRIMARY KEY (`Id`)
)
  ENGINE = MyISAM
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 5;

--
-- Dump dei dati per la tabella `Link`
--

INSERT INTO `Link` (`Id`, `Testo`, `Image`, `Site`) VALUES
  (1, 'POVEGLIA PER TUTTI', '/images/poveglia_logo.png', 'http://www.povegliapertutti.org/'),
  (2, 'ABC - ACQUA BENE COMUNE VENEZIA', '/images/ABC_logo.png', 'http://acquabenecomunevenezia.blogspot.it/'),
  (3, 'NO GRANDI NAVI', '/images/NoGN_logo.png', 'http://http://www.nograndinavi.it/'),
  (4, 'Link per per la pagina di Economia Solidale', '/images/eventhia_logo.png', 'http://www.eventhia.com');

-- --------------------------------------------------------

--
-- Struttura della tabella `MailList`
--

CREATE TABLE IF NOT EXISTS `MailList` (
  `Id`        INT(11) NOT NULL AUTO_INCREMENT,
  `Nome`      VARCHAR(80)      DEFAULT NULL,
  `Scopo`     TEXT,
  `Indirizzo` TEXT,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  UNIQUE KEY `Nome` (`Nome`)
)
  ENGINE = MyISAM
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 12;

--
-- Dump dei dati per la tabella `MailList`
--

INSERT INTO `MailList` (`Id`, `Nome`, `Scopo`, `Indirizzo`) VALUES
  (1, 'cannaregioalto', 'Mail list del sottogruppo di Cannaregio Alto', 'cannaregioalto@venezianogas.net'),
  (2, 'cannaregiobasso', 'Mail list del sottogruppo di Cannaregio Basso', 'cannaregiobasso@venezianogas.net'),
  (3, 'castello', 'Mail list del sottogruppo di Castello', 'castello@venezianogas.net'),
  (4, 'lido', 'Mail list del gruppo del Lido', 'lido@venezianogas.net'),
  (5, 'sanmarco', 'Mail list del sottogruppo di San Marco', 'sanmarco@venezianogas.net'),
  (6, 'sanpolo', 'Mail list del sottogruppo di San Polo', 'sanpolo@venezianogas.net'),
  (7, 'giudecca', 'Mail list del sottogruppo della Giudecca', 'giudecca@venezianogas.net'),
  (8, 'gruppo_accoglienza', 'Mail list del gruppo di Accoglienza', 'gruppo_accoglienza@venezianogas.net'),
  (9, 'gruppo_prodotti', 'Mail list del gruppo Prodotti', 'gruppo_prodotti@venezianogas.net'),
  (10, 'gruppo_sintesi', 'Mail list del gruppo di Sintesi', 'gruppo_sintesi@venezianogas.net'),
  (11, 'notizie', 'Mail list generale del Veneziano Gas', 'notizie@venezianogas.net');

-- --------------------------------------------------------

--
-- Struttura della tabella `MeseXProduttore`
--

CREATE TABLE IF NOT EXISTS `MeseXProduttore` (
  `Mese`       INT(11) NOT NULL DEFAULT '0',
  `Produttore` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Mese`, `Produttore`),
  KEY `Produttore` (`Produttore`)
)
  ENGINE = MyISAM
  DEFAULT CHARSET = latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `Mesi`
--

CREATE TABLE IF NOT EXISTS `Mesi` (
  `Id`       INT(11) NOT NULL AUTO_INCREMENT,
  `Name`     TEXT,
  `NameLong` TEXT,
  PRIMARY KEY (`Id`)
)
  ENGINE = MyISAM
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 13;

--
-- Dump dei dati per la tabella `Mesi`
--

INSERT INTO `Mesi` (`Id`, `Name`, `NameLong`) VALUES
  (1, 'Gennaio', 'Gen'),
  (2, 'Febraio', 'Feb'),
  (3, 'Marzo', 'Mar'),
  (4, 'Aprile', 'Apr'),
  (5, 'Maggio', 'Mag'),
  (6, 'Giugno', 'Giu'),
  (7, 'Luglio', 'Lug'),
  (8, 'Agosto', 'Ago'),
  (9, 'Settembre', 'Set'),
  (10, 'Ottobre', 'Ott'),
  (11, 'Novembre', 'Nov'),
  (12, 'Dicembre', 'Dic');

-- --------------------------------------------------------

--
-- Struttura della tabella `Permessi`
--

CREATE TABLE IF NOT EXISTS `Permessi` (
  `Id`    INT(11) NOT NULL AUTO_INCREMENT,
  `Label` TEXT,
  PRIMARY KEY (`Id`)
)
  ENGINE = MyISAM
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 4;

--
-- Dump dei dati per la tabella `Permessi`
--

INSERT INTO `Permessi` (`Id`, `Label`) VALUES
  (1, 'Gasista'),
  (2, 'Amministratore'),
  (0, 'Richiesta iscrizione'),
  (3, 'Altro');

-- --------------------------------------------------------

--
-- Struttura della tabella `Persona`
--

CREATE TABLE IF NOT EXISTS `Persona` (
  `Id`          INT(11) NOT NULL AUTO_INCREMENT,
  `Password`    TEXT,
  `Indirizzo`   TEXT,
  `Email`       TEXT,
  `Permessi`    INT(11)          DEFAULT NULL,
  `Telefono`    TEXT,
  `Cellulare`   TEXT,
  `Nome`        TEXT,
  `Sottogruppo` INT(11)          DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `Permessi` (`Permessi`),
  KEY `Sottogruppo` (`Sottogruppo`)
)
  ENGINE = MyISAM
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 205;

--
-- Dump dei dati per la tabella `Persona`
--

INSERT INTO `Persona` (`Id`, `Password`, `Indirizzo`, `Email`, `Permessi`, `Telefono`, `Cellulare`, `Nome`, `Sottogruppo`)
VALUES
  (1, '$P$BSSsunltdBTHsdB1ZWS23hdfS6hcGn/', 'S.Marco 4873', 'androsoft69@alice.it', 1, '041 2960609', '347 3700627',
   'Giordani Andrea', 2),
  (2, '$P$B2LD48cZf34.3oJHm2D9MLPq7wy8Nt1', 'S.Croce 1315', 'stefanobullo.gas@gmail.com', 2, '041 5235989', '338 69752352', 'Bullo Stefano', 5),
  (3, '$P$BfJFGBH8MV/QDWVoFO76szoLoWUY2Y1', 'Castello 1201', 'ugolazzari@gmail.com', 1, '041 5200191', '', 'Lazzari Ugo', 2),
  (4, '$P$BY.ByLu8xXEt0e2v1Oli2kL3Tc5HqJ.', 'S.Marco 4873', 'fracampagnoli@virgilio.it', 1, '041 2960609', '328 0440189', 'Campagnoli Francesca', 1),
  (5, '$P$BY35z6Ej6ApVc4Lodz9B7pPD5Fk.f81', 'Cannaregio 3092', 'soveeh@virgilio.it', 1, '041 5240667', '349 1494450', 'Franckel Elisabetta', 6),
  (6, '$P$BpizU2ldPwm6HG2vQ6ZwPY8vGMRNlL1', 'giudecca 944', 'cosima.roberto@gmail.com', 1, '041 5228379', '348 9248298', 'Bonivento Cosima Zanon Roberto', 4),
  (7, '$P$Bm5hjO3BVe9qlrG.lIOjaYspRrvV/H.', 'Santa Croce 312/A', 'mapi.giuponi@fastwebnet.it', 1, '041 713453', '346 0417274', 'Giuponi Maria Pia', 1),
  (8, '$P$B117ycS1xj9p47LKosrCeEcTZcpufM0', 'Cannaregio 5485', 'antonella.colussi@alice.it', 1, '041 5235989', '328 9247389', 'Colussi Antonella', 5),
  (9, '$P$BEFYo/0gVe2g7607toILOPOk.ZVlC31', 'S. Croce 1530', 'suppaman27@msn.com', 1, '', '320 1120970', 'Pontini Daniela', 1),
  (10, '$P$B2naRMgvEhpc0ukn6ZocdcOS2M.iw91', 'S.Marco 3982', 'paolosteffinlongo@yahoo.it', 1, '041 5229490', '328 1052483', 'Steffinlongo Paolo', 2),
  (11, '$P$BoXMfapgaW2zTBLzI9pwMm6TkMRelI.', 'S.Marco 2749', 'michela.siega@teletu.it', 1, '041 5229090', '329 5610300', 'Siega Michela', 2),
  (12, '$P$BI4ihfRlMsndBscYWLdIcN3/SEkroC1', 'Via F Morosini 14 Lido', 'pierpaolo.penzo@libero.it', 1, '041 5266344', '348 3325567', 'Penzo Pierpaolo', 3),
  (13, '$P$BRbJfwY6e0mrW8fm2KnIcTGrnC0Pri1', 'S.Marco 3198', '9marsedi@tin.it', 1, '', '328 4926919', 'Paipeti Olita', 2),
  (14, '$P$BPGF9Y6BYyXcL8ZDlDFf2ozctujb.71', 'S.Marco 3041', 'robi1506@gmail.com', 1, '041 5285212', '347 8251776', 'Corradini Roberta', 2),
  (15, '$P$B2u4x6or7rqKuMr0tti8nR9TGfoygn0', 'Castello 6282', 'piergiorgio.ve.gas@hotmail.it', 1, '041 5280308', '347 8471851', 'Dri Piergiorgio', 5),
  (16, '$P$Bi5lhscOfqA3fDc5Gpc7NtP28OCCG6.', 'S.Polo 2465/A', 'paolaissa@mclink.net', 1, '041 5285980', '333 7962687', 'Issa Paola', 1),
  (17, '$P$BM3BOSffhxoKpY58TbggmiBUJ.IINR.', 'S. Croce 1461', 'rifiutologo@hotmail.it', 1, '041 718153', '347 3361004', 'Santi Mario', 1),
  (18, '$P$BFOfoMwpW3EID3ymDPo5I828CYraIO/', 'Cannaregio 3501/d', 'andrea.rizzi@yahoo.it', 1, '041 5242657', '328 2178885', 'Rizzi Andrea', 6),
  (19, '$P$BbKt0j1gKtJYDZDHbENZZdISvjtMyc0', 'S.Croce 1154', 'silviaschiabello@hotmail.com', 1, '041 719214', '329 0508876', 'Schiabello Silvia', 1),
  (20, '$P$B3S.r9se1H9fQUE.ZTtQd6T7pTM3EV/', 'S.Marco 2794/A', 'giovanni.turchetto@gmail.com', 1, '041 5286132', '329 4114161', 'Turchetto Giovanni', 2),
  (21, '$P$BIOKxB3wL8fqVPqV3wkWJj.PMquekm1', 'San Polo 1949', 'frlsrn@unife.it', 1, '041 722940', '328 4909364', 'Forlati Serena', 1),
  (22, '$P$BPNlwDgeUhoYS/GbVdCT0MMlpgMwos/', 'S.Croce 1378', 'barbaraboifava@hotmail.com', 1, '', '348 1023684', 'Boifava Barbara', 1),
  (23, '$P$BAZAfL6GTSNfBMrKMnaDbEPzC.EAIR1', 'S. Croce 1530', 'glaz@libero.it', 1, '041 5229621', '347 6274266', 'Lazzarin Gloria', 1),
  (24, '$P$B25zTZMmzp8tN6VjzMY1Uom0AZlvGn1', 'DorsoDuro 2659', 'belligiu1@alice.it', 1, '', '338 1322176', 'Bellinaso Giuseppe', 1),
  (25, '$P$BRwd1df/3l0zMsFf2F9i698g2iX7160', 'Cannaregio 6272', 'ilazennaro@libero.it', 1, '041 5206274', '347 5638043', 'Zennaro Ilaria', 5),
  (26, '$P$BL6vlKUarDZftPWCgwh/1M7Rw1I1bP/', 'Cannaregio 5336', 'fabio.cummaudo@yahoo.it', 1, '041 5236454', '328 6838492', 'Cummaudo Fabio', 5),
  (27, '$P$BkNdXnMehQb9gpF7J9bVmEMucu4cJ71', 'S. Croce  999', 'vallipel@gmail.com', 1, '041 5242022', '338 7906699', 'Pellizzari Vall?', 1),
  (28, '$P$BhtTudCUVLJOjsANJYB25FdxszTrT.0', 'Cannaregio 1833', 'LoredanaDeOrazi@teletu.it', 1, '041 5240308', '', 'Loredana DeOrazi', 6),
  (29, '$P$BPxRlwft9ES0ZWPn6IHQgoQi.jSqLx1', 'S.Marco 4961', 'lucia.giacomazzi@gmail.com', 1, '', '339 8589087', 'Giacomazzi Lucia', 2),
  (30, '$P$BuP2QTU7PfwVtZ9QHZfPAQQwzBgwDm1', 'Cannaregio 936', 'amazza.ve@gmail.com', 1, '041 715630', '347 0571400', 'Mazza Anna', 6),
  (31, '$P$BX/wdTXKDbQD1HAMy32fiMuqycniWE1', 'Cannaregio 1269/a', 'camilla.scalet@virgilio.it', 1, '041 715527', '347 6954768', 'Scarlet Camilla', 5),
  (32, '$P$BwQC7hnBAHoDOtzm5fBkL6FZ925ynY1', 'Cannaregio 3931', 'e.fabris2@virgilio.it', 1, '041 5239360', '347 8064061', 'Fabris Elena', 6),
  (33, '$P$B9aVKU1JIw34vNh3mWxyeIM6sGhavO.', 'Cannaregio 3400/a', 'mariaf.levorato@tiscali.it', 1, '041 720038', '347 2706989', 'Levorato Francesca', 6),
  (34, '$P$BqJXF/.Wxrw7yUP/yqXOJDcn21Ksdd1', 'via enrico dandolo 39 lido', 'andreafant@yahoo.it', 1, '041 5260931', '340 2479923', 'Fant Andrea', 3),
  (35, '$P$B5cGWtvKgj6tpDkSSoOUTHJPXJOQcg.', 'Cannaregio 2251', 'fschenk@tin.it', 1, '041 720785', '335 6056634', 'Schenkel Franco', 6),
  (36, '$P$BQvSquhaENniTCjiemSPtlCGPQpLtb.', 'Via P.Paruta 14 Lido', 'lbarbieri60@gmail.com', 1, '041 5260330', '329 2105581', 'Barbieri Luigi', 1),
  (37, '$P$BmzxVZZ1nYikLQi1bUjAZswMjJO9bb1', 'Via Dardanelli 30 Lido', 'mlzordan@alice.it', 1, '041 2760381', '335 5723968', 'Zordan Maria Luisa', 3),
  (38, '$P$BcYqGq1AkOOVQsg6ZJbaJv0DU2.cKW.', 'Cannaregio 3718/c', 'nicola.calenda@gmail.com', 1, '', '340 5759598', 'Calenda Nicola', 5),
  (39, '$P$B6eul29F.zqb4b7XjVXCvVaKXyILsw/', 'F.ta Manin 53 Murano', 'sabinajuga@yahoo.it', 1, '041 5274837', '333 5809590', 'Serena Sabina', 5),
  (40, '$P$Bt/n/6so4qyeQlCOUhSWz4mfzPKgau/', 'S.Marco 3900', 'zenlaggia@gmail.com', 1, '041 5210033', '335 6624955', 'Rezai Rad Zen Barbara', 2),
  (41, '$P$B7EROFPXIA9c1RPTIKzPQlg5gt8NMG1', 'Via B. Stagnino 9/13', 'briciola88@gmail.com', 1, '041 770512', '328 5649844', 'Zambon Mariachiara', 3),
  (42, '$P$B7Og4mM.uppSBiskX4YkyaPQuGCuXA1', 'Via Dardanelli 30 Lido', 'avianeo@libero.it', 1, '041 2760381', '328 7424288', 'Vianello Angelo', 3),
  (43, '$P$Bq29Ab.D.037fXcFjdvR34OZn5.4lE0', 'Cannaregio 968', 'giulianarmani@outlook.it', 1, '041 716658', '328 1747591', 'Armani Giuliana', 6),
  (44, '$P$BEkjRGshScv.Zqqj7Nxlsssg08QkUy1', 'S.Polo 2362', 'chrderossi@gmail.com', 1, '041 8220344', '349 5339197', 'De Rossi Chiara', 1),
  (45, '$P$B30uu1YRg8UxuV9ro0MdaUlEQTQUeH0', '1 2184', 'ottaviano.barbanente@gmail.com', 1, '041 5241676', '347 8557666', 'Barbanente Ottaviano', 1),
  (46, '$P$BRWgRMkMgabCTsO6A0DyO05soY381F0', 'S. Polo 1028', 'antotron@gmail.com', 1, '041 5289225', '347 1176981', 'Masciovecchio Antonella', 1),
  (47, '$P$BUNPdKws58PBCyLNzPkeGhwqhyp.o21', 'Cannaregio 775', 'lucio.rosso@fastwebnet.it', 1, '041 713290', '320 2337749', 'Deprat Valentina', 5),
  (48, '$P$Bcpqsm/nFyt2MQZI7.Y0gwM2/...Ko.', 'Dorsoduro 3563', 'elyppa@libero.it', 1, '041 718429', '348 7902265', 'Andreoli Elisa', 1),
  (49, '$P$BnQ1dbv9NGwv32X5ankjFmYZjy.3qC.', 'Via Jacopo Nani 20 - 30126 Lido', 'danilosalvato@hotmail.com', 1, '041 5264557', '348 0883518', 'Salvato Danilo', 3),
  (50, '$P$BPiu1Xydo.PhouwuOdTnXYrC5ANe8h0', 'Cannaregio 2913', 'mabaldin@libero.it', 1, '041 710570', '339 3302579', 'Baldin Marina', 6),
  (51, '$P$BNK3Lyh8gPCLg8iyLv1DCE2yAWRWjK0', 'giudecca 891', 'fragas@live.it', 1, '041 5231784', '335 57741245', 'Tosi Francesca', 4),
  (52, '$P$BjJ9l6/SbJdpyaXj0CWY1sE8GKBM4W.', 'S. Croce 1495', 'bibip@libero.it', 1, '041 714518', '329 3652258', 'Pandiani Elisabetta', 1),
  (53, '$P$B6L76iekIaoFK6LEIxg7Si9JtpR8tu1', 'Cannaregio 4477', 'carlocapovilla@libero.it', 1, '041 5283367', '348 4762242', 'Capovilla Carlo', 5),
  (54, '$P$BTIZp3XQu5YM8lAN4Hnh/SknBDnJE8/', 'Castello 1201', 'peperi76@gmail.com', 1, '041 5200191', '348 4964190', 'Perissinotto Petra', 2),
  (55, '$P$B9OlBweeScTrFKgTteavvkyTohPt3E0', 'Dorsoduro 3936', 'gmencini@libero.it', 1, '041 719856', '335 5309595', 'Mencini Giannandrea', 1),
  (56, '$P$BE4FLWRZ126Bn6h/QISYinnx9sUJQu.', 'Dorsoduro 735/A', 'vianello.fra@gmail.com', 1, '041 5224459', '393 6395366', 'Vianello Francesca', 1),
  (57, '$P$B5imjGKANQjEufSSdQUdRpqS/eaRrE/', 'Cannaregio 2251', 'fsala@email.it', 1, '041 720785', '335 1051928', 'Sala Flavia', 6),
  (58, '$P$BR5kDPHqooiUtWA9yFhGFERVlfP0SI0', 'Cannaregio 1833', 'marinagavagnin@gmail.com', 1, '041 720356', '', 'Gavagnin Marina', 6),
  (59, '$P$BWLiawNYryLg5pfN/c1m15XIOeSRH1.', 'san polo2812', 'elispontoglio@libero.it', 1, '', '339 8548652', 'Pontoglio Elis', 1),
  (60, '$P$B4gRGk/oP78W1xlF47nPdpfPE5ehfQ1', 'Cannaregio 635/A', 'antonellamiarelli@gmail.com', 1, '041 722993', '347 6669672', 'Miarelli Antonella', 6),
  (61, '$P$BmCN3d.gzHZC/myhdaKwiY6tCSqbl01', 'Santa Croce 2206', 'archeoassociati@libero.it', 1, '041 2750790', '349 2968012', 'Cester Rossella', 1),
  (62, '$P$Bh7vji8A.1WZPOXqEpQhgIpjFgquNC/', 'Cannaregio 1866', 'gloriachat@hotmail.com', 1, '041 715581', '349 1242092', 'Grippo Maria Gloria', 6),
  (63, '$P$BchsrLnwtk2vljuHsEMFli3/mFyFlz.', 'S. Croce 1062/B', 'michela.orlando@sadop.it', 1, '', '333 2119511', 'Orlando Michela', 1),
  (64, '$P$Bm4fVIVOVjPDJMsdZKsBndhZkUpo5h0', 'Cannaregio2843', 'roberta.dis@teletu.it', 1, '041 722021', '335 6801379', 'Di Sanzio Roberta', 6),
  (65, '$P$BNzjbDYie7M4/pAvOJchiKmuaIYnzN0', 'Cannaregio3679', 'giocerni@hotmail.com', 1, '041 5229189', '', 'Cerni Giovanni', 5),
  (66, '$P$Bb6I8.NM5O3sDvvPFsgxQrDL0fN8gm.', 'S. Polo 1952', 'equaranta@inwind.it', 1, '041 5235174', '329 9784869', 'Quaranta Elena', 1),
  (67, '$P$BpWrMuwR0WozmcuQNTAvF5L6pcvo6N/', 'Cannaregio 1091/E', 'salvatorelopreiato@tin.it', 1, '', '349 5860875', 'Lopreiato Salvatore', 2),
  (68, '$P$BlXkgX96ADytTrB5Xb13KUjsZ5nHI6.', 'Castello 2364', 'martavergombello@yahoo.it', 1, '', '340 7013761', 'Vergombello Marta', 2),
  (69, '$P$BQc4HU.K4R4N1MC1XWhNlVnzD07jA01', 'Giudecca 937/c', 'mason.francesco@gmail.com', 1, '', '329 4961392', 'Mason francesco', 4),
  (70, '$P$B1WuVgsg6hvaF5JHtMy3blK1o1QufX0', 'santa croce 801', 'cottignola.marina@gmail.com', 1, '041 5224488', '338 8441092', 'Cottignola Martina', 1),
  (71, '$P$Bhxhd5ToEhJ2xf/TYUA/8OgHOlFwsZ1', 'Cannaregio 1345/d', 'chiara.colombini@tin.it', 1, '041 720161', '349 2219502', 'Colombini Chiara', 6),
  (72, '$P$BjdFXglcgPLgQY6I0q1tQf4sXjpuOu0', 'Cannaregio 2466', 'marina.vacciano@libero.it', 1, '041 721947', '', 'Pepe Marisa', 6),
  (73, '$P$Bvfsc.ri6sUMZqExFLTXmGik9RG/ct.', 's marco 3051', 'romamarina@gmail.com', 1, '041 5229850', '328 5383068', 'Roman Marina', 2),
  (74, '$P$B4cM8XH9Rx7i6WqYYJaZidU9xFVoSp0', 'S.Croce 1608', 'carlottavinanti@me.com', 1, '', '328 4227654', 'Vinanti Carlotta', 1),
  (75, '$P$BEBqLLMW/1CTeAHAH1ZRtjycv6mOXT1', 'S. Croce 1374', 'annamariasegna@libero.it', 1, '041 720898', '348 3050045', 'Segna Annamaria', 1),
  (76, '$P$B20PmKV6HFfZ.z7Qv3agdBymiJapro1', 'Cannaregio 2971/C', 'rosagiglio@hotmail.com', 1, '041/5244246', '339 5768017', 'Giglio Rosa', 5),
  (77, '$P$BTVWZC/Q/luz6GIXGJJcgHzA4Xfhv01', 'Castello 3133', 'agotosq@yahoo.it', 1, '041 5234407', '339 4113212', 'Tosques Agostino', 2),
  (78, '$P$BKigJ9OcGftdvTh0enYaHPzqcH5t19.', 'Cannaregio 2465', 'gavagnin.tiziana@yahoo.it', 1, '041 721897', '349 4158300', 'Gavagnin Tiziana', 6),
  (79, '$P$BEWgqF.8YOGcoDXt1fQyHp7sGruCH91', 'Via Hermada 17', 'margherita.brondino@univr.it', 1, '041 2410546', '335 7468399', 'Brondino Margherita', 5),
  (80, '$P$B2keVFdi18yWDLukT.5kaQWwD0NltD0', 'Cannaregio 2979/b', 'wondelfland@yahoo.it', 1, '', '347 9774270', 'Biral Enrico', 6),
  (81, '$P$B2qj0VT4cPwobVUvdWlSjl/0MCQZ3i.', 'Cannaregio 1310/c', 'luigi.amadio@tin.it', 1, '041 715152', '347 7986898', 'Testolin Serena', 6),
  (82, '$P$BAwY/7Bb3Lg9VbpPHRH0odSXq22tD9/', 'Cannaregio 5376', 'helis001@gmail.com', 1, '041 5209288', '347 5007545', 'Favaretti Elisabetta', 5),
  (83, '$P$B8B2eVlDEJq8hwNxUPsmec.pKg8jH8.', 'Cannaregio 472/c2', 'yaser.odeh@libero.it', 1, '041 710785', '347 0363245', 'Odeh Yaser', 6),
  (84, '$P$BU2UPJ2JfH10SZwt/QQA.YFBxuVDxY.', 'Cannaregio 6312', 'luigi.pruneri@fastwebnet.it', 1, '041 5233641', '348 4762243', 'Pruneri Luigi', 5),
  (85, '$P$B1QzYUjGx54oLR5.vM5AH/V8LZ/6/L1', 'S. Croce 791', 'nicolbatt@gmail.com', 1, '041 718891', '380 4533559', 'Battistel Nicoletta', 1),
  (86, '$P$BYqEpcNgCTFCIaEWRrvT/UR8xB7Rfl0', 'Cannaregio 2904', 'yazeed@mouseinvenice.it', 1, '041 714721', '329 4249228', 'El Labadie Yazeed', 6),
  (87, '$P$B1ATgHCozHy3T0wMQQgMhTTMFqDZ.T.', 'S. Croce 774', 'nenemancini@virgilio.it', 1, '041 718560', '338 5641872', 'Mancini Elena', 1),
  (88, '$P$BwnHKvQ4VYV99xrzUq.8C3ko0VduLd.', 'Giudecca 937/C', 'anna_brusarosco@yahoo.it', 1, '', '', 'Busarosco Anna', 4),
  (89, '$P$BBEIlh9D3btpjtTJRwxnA1r/qB27fe.', 'Cannaregio 2979/b', 'elena.volpato@hotmail.it', 1, '', '347 9774270', 'Volpato Elena', 6),
  (90, '$P$BzGbVPQu2kvTD9kIUjXE8IlVhIU970/', 'S.Marco 3702', 'annapizzati25@gmail.com', 1, '041 5222761', '347 3186771', 'Pizzati Anna', 2),
  (91, '$P$BQZGhrfWmcc6Glb.NEW9vkyx80KxIJ/', 'Viale 24 Maggio 13', 'giangoo3@gmail.com', 1, '041 5229980', '328 5687598', 'Trez Gianni', 2),
  (92, '$P$Bn72DOS0GoNmsNJKrku1Fu6auc.GoF/', 'Cannaregio 1863/h', 'lucillapiacentini@alice.it', 1, '041 713988', '347 1599575', 'Piacentini Lucilla', 5),
  (93, '$P$B4EVAlE.T6QmvExMpBQT6krONytBCB0', 'S. Croce 93', 'fioreru@libero.it', 1, '041 723174', '347 8469682', 'Ruffato Fiorella', 1),
  (94, '$P$BxIDnmXvLet92JInSdzIM9dCfu9Yrg.', 'Via 4 novembre s.elena', 'giuliaottaviani@libero.it', 1, '', '340 4967181', 'Ottaviani Giulia', 2),
  (95, '$P$BXvWjgVHA/bG18Ur4bUS9qajanh8ue0', 'S. Croce 853', 'archecco.bart@libero.it', 1, '041 2750083', '', 'Bartoloni Francesco', 1),
  (96, '$P$BoF964mXJZz44foopj53XYXmaSez7i1', 'Dorso duro 3490', 'mapibaruch@gmail.com', 1, '041 710540', '347 8790925', 'Barucchelli Mariapia', 1),
  (97, '$P$BugW4cQt5Z0QhGod6F5XBkuDIukFNa1', 'Dorsoduro 2776', 'ciaic74@yahoo.it', 1, '', '333 4898206', 'Martucci Ninetto', 1),
  (98, '$P$BWdG2PjBUqi1DcaZz5KxWnjq3vlNj7.', 'Cannaregio 500', 'nu.coco@alice.it', 1, '041 5242869', '347 2981259', 'Cecchetto Alessandra', 6),
  (99, '$P$B/Q1BrHW.ybgEOtfsdVaV8CsDZNqlT0', 'Cannaregio 1076/b', 'magicber@tin.it', 1, '041 5240604', '347 7258925', 'Berton Claudio', 6),
  (100, '$P$Bj29shwaFO/EVChhVLwEIqQI0gDDDw/', 'S.Croce 586', 'sandrapaol@hotmail.com', 1, '041 0990184', '', 'Paoli Sandra', 1),
  (101, '$P$Be.e7ezmSP8gF9kEU/m8b77egNKjTc.', 'S. Polo 3077 D', 'nizor36@gmail.com', 1, '041 5240365', '339 1225205', 'Zorzetto Nino', 1),
  (102, '$P$BJXg0EV1nkHcfeVO/UsmRAXYsDnqGo0', 'Cannaregio 1812b', 'apunter@iuav.it', 1, '041 721624', '328 0391806', 'Punter Antonella', 6),
  (103, '$P$B10h/9MvJRS9f41c7DsOFTbEDJwAfP0', 'Castello  4428/A', 'gi.raffa.ele@libero.it', 1, '041 2410941', '333 5281759', 'Riccardi  Manuela', 2),
  (104, '$P$BKB/D4yVtwem9jf1oRyqpSUUC3r47B/', 'San Polo 2030', 'sandragori@gmail.com', 1, '041 714803', '', 'Sambo Paolo', 1),
  (105, '$P$BJEKXVXGhJPp2ekNAcc6GG2fG5UXFw1', 'giudecca 688 e', 'nicola_bortolozzi@yahoo.it', 1, '', '340 2988561', 'Bortolozzi Nicola', 4),
  (106, '$P$BiY/yAZGT9SR1FrtwWJSbvN9KflkW/1', 'Castello 1685', 'niero@ateneoveneto.org', 1, '041 5221894', '347 7164041', 'Niero Marina', 1),
  (107, '$P$BI8d1lSo.J1iQeqmiSb4RIQd7o.MoV/', 'Castello 651', 'annserr@tiscali.it', 1, '041 5222167', '333 5088581', 'Serra Anna', 2),
  (108, '$P$BGnPgNYmiHBxGCQ.C4KIgjhOwgu7AS.', 'Castello 3207', 'mariaanna.vanuzzo@fastwebnet.it', 1, '041 5208166', '339 4165461', 'Vanuzzo Mariella', 2),
  (109, '$P$B8QObLiGkIkxbcjPkZ3nlDFBSESOkb.', 's. croce 1677', 'audida@libero.it', 1, '', '342 1770524', 'Rizzo Stefania', 1),
  (110, '$P$BMF9ezLlWTTLmc2eHQFh30CMvDUN0g.', 'Castello 4696', 'gdarai@libero.it', 1, '041 5236028', '328 3760333', 'Darai Gianni', 2),
  (111, '$P$BqVV6HFPcXGroHpzhMtZ.r1V0IdqD71', 'S.Marco 3898', 'benedetta_bortol@hotmail.com', 1, '041 5238498', '338 7613235', 'Bortoluzzi Benedetta', 2),
  (112, '$P$BRiHSNoLmfvbwL0YtARoNDpUY0MwSz0', 'Castello 4226', 'catezanon@gmail.com', 1, '041 5208887', '329 6495489', 'Zanon Caterina', 5),
  (113, '$P$BOZf1ScCAtAkN2LBlDw4Rxk/G7g0aL/', 'S.Marco 1074', 'acioacio@libero.it', 1, '041 5206110', '349 0076682', 'Nicoletti  Maria Grazia', 2),
  (115, '$P$BEdKlD1nAmva1OuYqXQfyATr60GUN8/', 'Cannaregio 3179', 'bdelme@hotmail.com', 1, '041 5246324', '333 2723520', 'Del Mercato Barbara', 6),
  (116, '$P$BpKyzoYniUVGX6CTdfy6e5VgGyzp01/', 'Castello 3463', 'lorenzoc@unive.it', 1, '041 5223046', '', 'Calvelli Lorenzo', 2),
  (117, '$P$BATBRmG2M8h.fJ.k9f69iF/EoNHlKe1', 'Giudecca 204/b', 'lieslodenweller@gmail.com', 1, '041 2750564', '347 3110194', 'Odenweller Liesl', 4),
  (118, '$P$BLFf8YLFJQbvtDdGpSgFy3BUMUjhnP/', 'Castello 1247/a', 'guidonia12013@yahoo.it', 1, '041 2770852', '340 3375579', 'Boscolo Guido', 2),
  (119, '$P$BgAgXm1bzpmUs1vKi6HDf.KG8MoC75/', 'Cannaregio 1075/a', 'acetik@fastwebnet.it', 1, '041 717038', '328 4677612', 'Rossi Andrea', 6),
  (120, '$P$Buf5MIxo7B3KfUb8nGzSdntCtYz1J1/', 'Dorso duro 3488/r', 'giovanniscavella@libero.it', 1, '', '320 6806774', 'Scavella Giovanni', 1),
  (121, '$P$BhkoJtkW/aYiNF0Pem8VmPA1xfrFzx1', 'S.Polo 1617', 'mariarita.gennaro@libero.it', 1, '041 5246192', '328 0593199', 'Gennaro Mariarita', 1),
  (122, '$P$BZ6lO13n.rVkco3SN.HwVfWH8Wru.60', 'Cannaregio 3718/c', 'federica.napolitano@gmail.com', 1, '', '347 7615539', 'Napolitano Federica', 5),
  (123, '$P$B/hwQQ5twlXjutTVn8wywv/gyUaAX70', 'Castello 2040', 'jlouis79@hotmail.com', 1, '041 5285490', '348 3737091', 'Cianchetti Riccardo', 2),
  (124, '$P$BeeJtrJhZtiRqvRjUn6epRLuioLBal0', 'S. Croce 1772', 'manugaia@alice.it', 1, '', '347 7682494', 'Benedetti Manuela', 1),
  (125, '$P$BiKnL4Fj12MGrKSCht0gghZdvhv0Ao1', 'Santa Croce 1636', 'giulianafarinati@yahoo.com', 1, '', '347 7223666', 'Simoni-Farinati Giuliana', 1),
  (126, '$P$B7hjy25awqom11P6wIT61GYABeebj.0', 'Cannaregio 3406', 'cm.meneghini@gmail.com', 1, '041 721821', '347 8618482', 'Meneghini Carla', 6),
  (127, '$P$BEZHWbfa6ZHoaorkchI8ABOK2yoaMl0', 'giudecca 189', 'cerpellonianitamarin@libero.it', 1, '041 5227203', '348 4449093', 'Cerpelloni Anita', 4),
  (128, '$P$BlRe3e7nEiqcWIRF07ITvQeq6M/RjW.', 'giudecca 189', 'robertapierobon@virgilio.it', 1, '', '328 9139394', 'Pierobon Roberta', 4),
  (129, '$P$BEyQtnq2Ph0AJYvCfEkF9Pabb8XJ0e1', 'S.Croce 1373', 'fredloca@yahoo.it', 1, '', '329 6086644', 'Locatelli Federica', 1),
  (130, '$P$BBe/3MhWf8vv7PstVAEpjzs5N8T1Db.', 'Castello 164', 'cb.dialogos@gmail.com', 1, '041 5228074', '377 1603001', 'Battistin Carla', 2),
  (131, '$P$BiMdILiWHNnip7hlM/T5lZI.CaM.NU0', 'Via Sabellico 6 -30126 Lido', 'silvia.chemollo@gmail.com', 1, '041 770558', '349 2837638', 'Chemollo Silvia', 3),
  (132, '$P$BBOBag.zmW67.QEYE7xFMWBuIYO85i0', 'Cannaregio 1240', 'antonella_deluigi@alice.it', 1, '041 740968', '347 0508113', 'De Luigi Antonella', 6),
  (133, '$P$BZ8JjKQUBNyfRTRJYkqkqVUMQPbwBo/', 'S.Polo 2899', 'francescoboldrin@yahoo.it', 1, '041 5229868', '329 1605531', 'Boldrin Francesco', 1),
  (134, '$P$BvyiTil4SNEenrxKOBhqusMY3QkxUh1', 'Via E. Sorgen 2 30126 Lido', 'nataliahime@libero.it', 1, '041 8392790', '347 1166067', 'De Tollenaere Nathalie', 3),
  (135, '$P$BYEFm56P91yqeChJtRH8XzhhdrwUVX/', 'giudecca 38', 'gdluigi@libero.it', 1, '041 5222941', '', 'Cavagnaro Anna', 4),
  (136, '$P$BIxskRXZrzin5dc6YXujAPO8z2hcml1', 'giudecca 939', 'jrbrandmeyer@hotmail.com', 1, '041 5231129', '328 1766564', 'Brandmeyer  Joern', 4),
  (137, '$P$B36/HlYYtYOr.8BM/FayY0no60XZCT0', 'Piazza Delle Erbe 3 - 30126 Lido', 'formusociro@virgilio.it', 1, '041 5261627', '347 3705783', 'Formuso Ciro', 3),
  (138, '$P$BK07AJxhY9jo2YAIBRGuXAKT7Hfbxd.', 'Calle Sacca 4', 'fiorotmarialaura@gmail.com', 1, '', '347 4705153', 'Fiorot Maria Laura', 4),
  (139, '$P$BB4cFABFKVe/YVCgqDroivwXSWiMI.1', 'Dorsoduro 1046', 'eddysal@tin.it', 1, '041 5223191', '', 'Salzano Eddy', 1),
  (140, '$P$BW7oOx0uv7JIGtEP9Nnaob/pJ3657s1', 'Via Cipro 3', 'marina.deste@alice.it', 1, '', '339 4013941', 'D Este Marina', 3),
  (141, '$P$BgjRIlRHATaZcsb.9SOPgHyCuWTzA4.', 'DORSODURO 3464', 'paola.salmini@cersal.it', 1, '', '335 7001461', 'Salmini Paola', 1),
  (142, '$P$BEui54pD9YlLAba0CBr2mIbd5QIOtU/', 'S.Polo 3078/R', 'laura.ceriolo@gmail.com', 1, '041 8227238', '328 9898870', 'Ceriolo Laura', 1),
  (143, '$P$BmESU23gNqLCFZS36u5IgrRG7k01rU.', 'Cannaregio 2951 e', 'laurapajer@gmail.com', 1, '', '349 8730601', 'Pajer Laura', 6),
  (144, '$P$BN.tsiB2n35XpFERlwQfzPU5axeZn51', 'S.Marco 867', 'valentina.mazzetto@hotmail.it', 1, '', '348 4379429', 'Mazzetto Valentina', 2),
  (145, '$P$BsGFSMSP6ilThYW9pyYdUTm6udKb/a/', 'S.Polo 2666', 'mariarosaria.barba@fastwebnet.it', 1, '041 5224824', '347 4534437', 'Barba Mariarosaria', 1),
  (146, '$P$B2Iy8OF.5tGJriQDwX8Fhmv2y14kcu/', 'Cannaregio 3286', 'magdalena.delprete@gmail.com', 1, '041 0991385', '342 6385463', 'Del Prete Anna Magdalena', 5),
  (147, '$P$BezOxuS64SDUsJwmstTxzFnRpRq4NO.', 'dorsoduro', 'bassirandigas@gmail.com', 1, '041 5204112', '340 5846980', 'Bassirandi Letizia', 1),
  (148, '$P$Bdqn7CXC3oYgsHDpvDXYUfgzm9G.jL/', 'giudecca 937/a', 'andfav@inwind.it', 1, '041 5238351', '328 8146402', 'Favaro Andrea', 4),
  (149, '$P$BtSurOv.v5p5m0SEiQOH8O/CAD00UC/', '', 'meomrz@libero.it', 1, '', '', 'Meo Maurizio', 3),
  (150, '$P$BmHGHDd38EOqj.NjNMILmuYimkeEDK1', 'Dorsoduro 2001', 'gianc@iuav.it', 1, '041 200247', '328 2255019', 'Fuga margherita e Gianni', 1),
  (151, '$P$BDv/d4ARNhATtBU1RdjHI1WyUXsA7o0', 'Via Barbarigo 1/d', 'luca.furlan@inwind.it', 1, '041 5267370', '346 0312335', 'Furlan Luca', 3),
  (152, '$P$Bf35QY4p8kk3GVqrvGG6mmdSEVjRQ01', 'S.Marco 3657', 'andrea.palermo@gmail.com', 1, '', '340 2896348', 'Palermo Andrea', 2),
  (153, '$P$B28K58cJVC3zXRf2f1g4xgT.VM9qUP/', '', 'mimisite@yahoo.ca', 1, '', '', 'Spinelli Mario', 2),
  (154, '$P$BcQrQG0NY80R0gH6B2mKzq0thSo2X51', '', 'ferrante.paolo64@lgmail.com', 1, '', '', 'Ferrante Paolo', 3),
  (155, '$P$BykR8277DJM8uuyyuuDg/0RukL5/7G1', 'Via Doge Michiel 10 Lido', 'susannasan@libero.it', 1, '041 2420061', '346 8534349', 'Sibaldi Susanna', 3),
  (156, '$P$Bg5p6ooDgDYOsNhAABitycOHrrbTBQ.', 'Castello 3196', 'paolamodesti@gmail.com', 1, '', '348 2590987', 'Modesti Paola', 2),
  (157, '$P$BeZeI6Bto9SKwZoSqN.8WuxPi4deru1', 'Cannaregio 549', 'linda.suran@gmail.com', 2, '', '320 2527545', 'Suran Linda', 6),
  (158, '$P$BgPfMfQF1lT7GBoNnEgMhj3SXKPNmn1', 'S. Croce 600', 'martina.giuponi@gmail.com', 1, '', '338 1221417', 'Giuponi Martina', 1),
  (159, '$P$BnEqAuN6PNyaVsP24BCeM.JvOBCeRU0', 'Giudecca 482', 'sarpedrini@gmail.com', 1, '', '328 5849402', 'Pedrini Sara', 4),
  (160, '$P$BZt3BJJjQRTsv.hi8Oj/FCNaiXMfpl0', 'Cannaregio 1447/a', 'francesca.giuponi@gmail.com', 1, '041 5244084', '', 'Giuponi Francesca', 5),
  (161, '$P$BK9aofCudFFD5UlcLHgEk.OjQM/o9q/', 'S.Polo 842', 'monbarg@tin.it', 1, '041 5243889', '348 9533369', 'Bargigli Monica', 1),
  (162, '$P$BpPyCyUoArsJJAiK/0y14cO45ScCux1', 'Castello 4519', 'maria.angelopoulou@libero.it', 1, '', '329-3334340', 'Angelopoulou Maria', 2),
  (163, '$P$Brxp8nthnFBzmpQbp.IZRMxHav1a2F1', 'San Polo 2725', 'giovincinz@gmail.com', 1, '041 5244000', '338 1681193', 'Giovine Cinzia', 1),
  (164, '$P$BrzQTQ5tlWTZzsSf0Fw1cvFSXTEZex1', 'Cannaregio 5049', 'cate.zannoni@gmail.com', 1, '041 5208887', '349 8735982', 'Zannoni Caterina', 5),
  (165, '$P$BGxCu/BEW58HVE/R9PpSABhrSeCSlP/', 'Cannaregio 2969', 'alvirossi@gmail.com', 1, '041 2440267', '380 3290290', 'Rossi Alvise', 6),
  (166, '$P$BILNQJ00ekb6Pii771huksw2w31OPz1', 'Cannaregio 2408', 'timon77@hotmail.it', 1, '041 5240318', '', 'Fusaro Emma', 6),
  (167, '$P$BHJjb7T6A8q8F8gRaFgmakq8YY4c3I0', 'Cannaregio 2399', 'robbemariapia@alice.it', 1, '041 715125', '347 8471828', 'Robbe Maria Pia', 6),
  (168, '$P$BMt0xWKFdZDSH./xBK2E/1hfmk3NAy/', 'S.Polo 1519', 'jannross@libero.it', 1, '041 718337', '349 6353634', 'Janna Rossana', 1),
  (169, '$P$BJgHy6AL2Gp8ve2Cpwl/Sk3harMTLc1', 'santa croce 93', 'matteoscarpa92@gmail.com', 2, '041 723174', '347 3361689', 'Scarpa Matteo', 1),
  (170, '$P$B8x8CIrkJsejI11IzCr.aG0eJDJ7NJ.', 'castello 2330a', 'emabonifacic@gmail.com', 1, '', '377 4485937', 'Bonifacin Ema', 2),
  (171, '$P$BEw4pB7x69wv5Ymo9iFxooJhvO9Rb4.', 'S. Croce 1317', 'mariachiara228@gmail.com', 1, '', '347 3812960', 'Vio Maria', 1),
  (172, '$P$B0E9W5M/y5SH3nWQzvon5JgT8gqk3A1', 'Via Duodo n. 17 Lido', 'angelapasculli3@gmail.com', 1, '', '347 0790479', 'Pasciulli Angela', 3),
  (173, '$P$B7apVD6unIv7xmkUtD7h8zDn/iOuqF1', 'Cannaregio 2013/B', 'luise.aless@gmail.com', 1, '041 714860', '333 2308327', 'Luise Alessandra', 6),
  (174, '$P$BQh8N6t3uviJXAtO22gJHlXzUFu/mz/', 'S.Marco 3034', 'info@studiomanca.org', 1, '', '346 5381410', 'Manca Paola', 2),
  (175, '$P$B6RZPSSut5GLDASAgpx6UfeeZ8778b/', 'Cannaregio 2584', 'filippo.napoletano@finint.com', 1, '', '335 6366286', 'Napoletano Filippo', 6),
  (176, '$P$B8OGP1Ib63EXDLmoSKX7Dwmwl1ST3X1', 'S.Marco 3507', 'lunare185@gmail.com', 1, '041 5232381', '347 1667547', 'Toniato Adriana', 2),
  (177, '$P$BFmahO4ke9KAo7H/Wx2crlbAkpQBUr/', 'Cannaregio 3846/a', 'monicafaevenezia@gmail.com', 1, '041 5208293', '347 8471795', 'Fae Monica', 6),
  (178, '$P$BZKHdEnqTCrmRNU85BAmY3wxRO1gvU/', 'Cannaregio 3027/m', 'giacolombini@gmail.com', 1, '', '333 2580104', 'Colombini Giacomo', 6),
  (179, '$P$BZXJmKvWiCVnMdiMgMkV767Zl8pZWz.', 'Dorso duro 3553', 'enzo.gnone@fastwebnet.it', 1, '', '329 3022891', 'Gnone Enzo', 1),
  (180, '$P$BP2rCsAPaviK4Y2MBTuSuDyOOIh/hO1', 'castello 881/b', 'adadami@gmail.com', 1, '041 5231420', '339 5313257', 'Adami Daniela', 2),
  (181, '$P$BqgcvsPo2X1Rk/A5EGXCED87qt6Pi3/', 'S.Croce 1669', 'manfredi.vale@gmail.com', 1, '', '328 5828251', 'Vale Manfradi', 1),
  (182, '$P$BqmeBEhCB7KcK6NEaGEDzDPWdeWxj0.', 'Giudecca 877', 'marinella.davide@gmail.com', 1, '', '328 9641922', 'Davide Marinella', 4),
  (183, '$P$BahENKhM/n1KBpvYgw2QSPFOZK/5Xa/', 's.polo 2490', 'grisonelisa@libero.it', 1, '', '349 5762864', 'Grison Elisa', 1),
  (184, '$P$ByNBzx0WlXftjPtF6gnddzAP2a3M.k/', 's.marco 3951', 'umberto.r@inwind.it', 1, '', '328 3016279', 'Rosin Umberto', 2),
  (185, '$P$BJ.f7G83ZiKdjlw5HuL/BxHmK7aGMn0', 'S.Croce 2279', 'leti.palazzetti@gmail.com', 1, '', '339 3734911', 'Palazzetti Letizia', 1),
  (186, '$P$BZB.QtLsRWDBJQ85qxen9gs.W5I12t1', 'Cannaregio 2819/B', 'marghebaggio@gmail.com', 1, '', '340 0053433', 'Baggio Margherita', 6),
  (187, '$P$BjzoATJveFy3uBoCxuNeLKAJoCjzUB/', 'via A Paganino 91 Lido', 'pilotsandro2@gmail.com', 1, '041 770786', '347 8840190', 'Sandro Pilot', 3),
  (188, '$P$BBH77riUitrtXPBvPtbWc2XdseaPFJ1', 'via A. Manuzio 7 C Lido', 'ferrolisaelena@gmail.com', 1, '', '328 3513956', 'Lisa Ferro', 3),
  (189, '$P$BaTtC7vYCd/NNG2p1NJ6RW3p2tMQnJ0', 'via Smirne 9/a', 'mariotto.carla@libero.it', 1, '', '340 2980116', 'Carla Mariotto', 3),
  (190, '$P$BGCGYXgQEy7YAXwAsDp9RtAYNpZyWC.', 'Calle sacca n.2 Giudecca', 'karlottacontin@libero.it', 1, '', ' 347 9128229', 'Contin Carlotta', 4),
  (191, '$P$B7FMt7eV9nN.1dX3iv3i3AJUwcZ1r9/', 'Giudecca 515', 'federicabezzoli@libero.it', 1, '041 2000328', '348 9595974', 'Bezzoli Federica', 4),
  (192, '$P$B/RbK3GAFl/9Sjyhgi6rFpBC7kSFEs.', 'Castello 3901', 'riccardochiarot@gmail.com', 1, '', '348-3840555', 'Chiarot Riccardo', 2),
  (193, '$P$BWaWYPGR9j9fGnZoIg.KotfOAtuIb0.', 'Cannaregio 549', 'palazzoli.chiara@gmail.com', 1, '041 952210', '347 0828580', 'Palazzoli Chiara', 6),
  (194, '$P$B4WawJYNxVQV9rQbYHOJcft6G3bcrL/', '', 'silvia221@gmail.com', 1, '', '347 1719921', 'Scarpa Silvia', 6),
  (195, '$P$BkFghiKLWfpQXiIyxQqJKl1kYLsU3o.', 'Castello 2602', 'bc.borsato@gmail.com', 1, '', '320 3287758', 'Borsato Barbara', 2),
  (196, '$P$BvtUVjgwGoe3a49I.ubmef5U47eHPl1', 'S.Marco 3559', 'licia.lucchese@virgilio.it', 1, '041 5226982', '349 2133817', 'Lucchese Licia', 2),
  (197, '$P$B6fz.3yRPosEAw1tL1dB2G5aD2ynH4.', 'S. Croce 847/C', 'f.moretti@email.it', 1, '338 6227209', '349 3744815', 'Moretti Filippo', 1),
  (198, '$P$BB3DmsB4zo6JNBfzSPRTLP8/8W7ECH/', 'castello 5102', 'naninese@gmail.com', 1, '', '349 5894650', 'Dainese Nadia', 2),
  (199, '$P$BoAeRa6JqvQ/Ac8SW4z/CUz8oyuYH40', 'castello 97', 'ellie.zachariadou@gmail.com', 1, '', '339 8679153', 'Zachariadou Ellie', 2),
  (200, '$P$BkAQmJYd0LBnTyvLqER1GLRc0ZtLsy.', '2 2128', 'avvmariaelenamancuso@gmail.com', 1, '', '338 1907230', 'Mancuso Maria Elena', 2),
  (201, '$P$BhU4cswTXR99XnBuJ1LaRdSns9l7oV.', 'Via Bruno Buozzi 20 Mestre', 'gabriele.regini@gmail.com', 1, '', '340 7307909', 'Regini Gabriele', 5),
  (202, 'MANCANTI', 'Cannaregio 96/e', 'p_criscione@hotmail.com', 1, '041 719017', '348 1515024', 'Criscione Paolo', 6),
  (203, 'MANCANTI', 'S. Polo 2630/A', 'laura.riganti@telecomitalia.it', 1, '041 7701912', '335 1357754',
   'Riganti Laura', 1),
  (204, 'MANCANTI', 'Dorsoduro 1046', 'ilariaborriburini@tin.it', 1, '041 5223191', '335-5885338', 'Boniburini Ilaria',
   1);

-- --------------------------------------------------------

--
-- Struttura della tabella `Produttore`
--

CREATE TABLE IF NOT EXISTS `Produttore` (
  `Id`          INT(11) NOT NULL AUTO_INCREMENT,
  `Name`        TEXT,
  `Prodotto`    TEXT,
  `Listino`     INT(11)          DEFAULT NULL,
  `Dove`        TEXT,
  `Quando`      TEXT,
  `Certificato` TEXT,
  `Modalita`    TEXT,
  `Sito`        TEXT,
  `Categoria`   INT(11)          DEFAULT NULL,
  `Immagine`    TEXT,
  PRIMARY KEY (`Id`),
  KEY `Categoria` (`Categoria`),
  KEY `Listino` (`Listino`)
)
  ENGINE = MyISAM
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 30;

--
-- Dump dei dati per la tabella `Produttore`
--

INSERT INTO `Produttore` (`Id`, `Name`, `Prodotto`, `Listino`, `Dove`, `Quando`, `Certificato`, `Modalita`, `Sito`, `Categoria`, `Immagine`)
VALUES
  (1, 'Az Ag Biologica Tordonato Corrado', 'Arance', 24, 'punti di ritiro sparsi nel territorio', NULL, NULL,
      'sito Economia Solidale', NULL, 2, '/images/img_prodotti/arancia.png'),
  (2, 'Az Ag Altaura e Monte Ceva', 'Carne Bovina', NULL, 'Arrivo a P le Roma', NULL, NULL, 'sito Economia Solidale', NULL, 7, '/images/img_prodotti/carne.png'),
  (3, 'Papaveri e Paperi', 'Carne Suina', 18, 'Arrivo a P le Roma', NULL, NULL, 'sito Economia Solidale', NULL, 7, '/images/img_prodotti/carne.png'),
  (4, 'Officina Naturae', 'Detersivi', 15, 'punti di ritiro sparsi nel territorio', NULL, NULL, 'sito Economia Solidale', NULL, 9, '/images/img_prodotti/detersivi2.png'),
  (5, 'Ass El Fontego', 'Equo solidale', 12, 'Ritiro a casa del RdP', NULL, NULL, 'sito Economia Solidale', NULL, 3, '/images/img_prodotti/biscotti_mandorle.png'),
  (6, 'Az Ag Le Barbarighe', 'Farine cereali', 11, 'Arrivo a P le Roma', NULL, NULL, 'sito Economia Solidale', NULL, 3, '/images/img_prodotti/farina.png'),
  (7, 'Az Ag Sermondi', 'Formaggio', NULL, 'Arrivo a P le Roma', NULL, NULL, 'sito Economia Solidale', NULL, 8, '/images/img_prodotti/formaggio.png'),
  (8, 'Biogold', 'Formaggio Parmigiano', 19, 'Ritiro a casa del RdP', NULL, NULL, 'sito Economia Solidale', NULL, 8, '/images/img_prodotti/formaggio.png'),
  (9, 'Persegona', 'Formaggio Parmigiano', 20, 'Arrivo a P le Roma', NULL, NULL, 'sito Economia Solidale', NULL, 8, '/images/img_prodotti/formaggio.png'),
  (10, 'Mele Antiche', 'Mele Antiche', NULL, 'Arrivo a P le Roma', NULL, NULL, 'via mail al rdp', NULL, 2, '/images/img_prodotti/mela.png'),
  (11, 'Lucano Masseria Perillo – Prodotti Lucani', 'Olio', NULL, 'punti di ritiro sparsi nel territorio', NULL, NULL, 'sito Economia Solidale', NULL, 6, '/images/img_prodotti/olio.png'),
  (12, 'L’agricola Paglione di Albano Maria Costanza & Matteo', 'Olio Passata e Pelati', 16, 'punti di ritiro sparsi nel territorio', NULL, NULL, 'sito Economia Solidale', NULL, 6, '/images/img_prodotti/olio.png'),
  (13, 'El forno a legna (Claudio)', 'Pane', 17, 'Ritiro a casa di chi di turno', NULL, NULL, 'Al referente di zona', NULL, 3, '/images/img_prodotti/pane.png'),
  (14, 'La Terra e il Cielo soc agr  arl', 'Pasta e legumi', 21, 'Punti di ritiro sparsi nel territorio', NULL, NULL, 'su documenti di google', NULL, 4, '/images/img_prodotti/pasta.png'),
  (15, 'Campoverde', 'Polli', 8, 'Arrivo a P le Roma', NULL, NULL, 'sito Economia Solidale', NULL, 7, '/images/img_prodotti/carne.png'),
  (16, 'Az Ag La Fornasa', 'Riso', 23, 'Arrivo a P le Roma', NULL, NULL, 'sito Economia Solidale', NULL, 4, '/images/img_prodotti/riso.png'),
  (17, 'EOS S r l', 'Saponi', 13, 'Ritiro a casa del RdP', NULL, NULL, 'sito Economia Solidale', NULL, 9, '/images/img_prodotti/saponi.png'),
  (18, 'Cooperativa Rio tera dei pensieri', 'Verdure', NULL, 'Ritiro al Carcere femminile', NULL, NULL, 'direttamente al referete', NULL, 2, '/images/img_prodotti/verdura.png'),
  (19, 'Az Agricola G Serafin', 'Verdure', NULL, 'Arrivo a P le Roma', NULL, NULL, 'direttamente al produttore', NULL, 2, '/images/img_prodotti/verdura.png'),
  (20, 'Az Agricola Farina', 'Clementine', 9, 'punti di ritiro sparsi nel territorio', NULL, NULL, 'sito Economia Solidale', NULL, 2, '/images/img_prodotti/clementine.png'),
  (21, 'Astor Flex', 'Scarpe', NULL, 'A casa del RdP', NULL, NULL, 'via mail al referente', NULL, 10, '/images/img_prodotti/scarpe.png'),
  (22, 'Az Agricola Aldo Bustaffa', 'Miele', 14, 'Arrivo a P le Roma', NULL, NULL, 'sito Economia Solidale', NULL, 5,
       '/images/img_prodotti/miele.png'),
  (23, 'Val Doga', 'Pesce', 22, 'Arrivo a P le Roma', NULL, NULL, 'sito Economia Solidale', NULL, 7,
       '/images/img_prodotti/Pesce.png'),
  (24, 'SOS Rosarno', 'Agrumi', NULL, 'punti di ritiro sparsi nel territorio', NULL, NULL, 'sito Economia Solidale',
       NULL, 2, '/images/img_prodotti/arancia.png'),
  (25, 'Bruno de Biase', 'Arance Gargano', NULL, 'Arrivo a P le Roma', NULL, NULL, 'sito Economia Solidale', NULL, 2,
       '/images/img_prodotti/arance_amare.png'),
  (26, 'Rossetto', 'Vino', 26, 'Arrivo a P le Roma', NULL, NULL, 'sito Economia Solidale', NULL, 6,
       '/images/img_prodotti/vino.png'),
  (27, 'Carcere Rio Tera dei Pensieri', 'Cosmesi', NULL, 'Arrivo a P le Roma', NULL, NULL, 'sito Economia Solidale',
       NULL, 9, '/images/img_prodotti/Oli-Essenziali.png'),
  (28, 'Verdure Sant Erasmo', 'Verdura', NULL, '', NULL, NULL, '', NULL, 2, '/images/img_prodotti/verdura.png'),
  (29, 'Erbamadre', 'Olii essenziali', NULL, 'Ritiro a casa del RdP', NULL, NULL, 'Via mail', NULL, 9,
       '/images/img_prodotti/Oli-Essenziali.png');

-- --------------------------------------------------------

--
-- Struttura della tabella `Referente`
--

CREATE TABLE IF NOT EXISTS `Referente` (
  `Persona`    INT(11) NOT NULL DEFAULT '0',
  `Produttore` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Persona`, `Produttore`),
  KEY `Produttore` (`Produttore`)
)
  ENGINE = MyISAM
  DEFAULT CHARSET = latin1;

--
-- Dump dei dati per la tabella `Referente`
--

INSERT INTO `Referente` (`Persona`, `Produttore`) VALUES
  (21, 16),
  (31, 20),
  (33, 25),
  (57, 28),
  (62, 13),
  (66, 14),
  (84, 1),
  (181, 6);

-- --------------------------------------------------------

--
-- Struttura della tabella `Ruoli`
--

CREATE TABLE IF NOT EXISTS `Ruoli` (
  `Nome`    TEXT,
  `Id`      INT(11) NOT NULL AUTO_INCREMENT,
  `Persona` INT(11)          DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `Persona` (`Persona`)
)
  ENGINE = MyISAM
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 9;

--
-- Dump dei dati per la tabella `Ruoli`
--

INSERT INTO `Ruoli` (`Nome`, `Id`, `Persona`) VALUES
  ('Cassa', 1, 7),
  ('Gestione posta Veneziano Gas', 2, 60),
  ('Comunicazione e rapporti altri GAS', 3, 35),
  ('Retegas del veneziano', 4, 35),
  ('Retegas del veneziano', 5, 67),
  ('Amministratore sito Venezianogas', 6, 1),
  ('Amministratore Eventhia', 7, 2),
  ('Amministratore sito nuovo Venezianogas', 8, 157);

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
