<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : pagina ufficiale : Proponi i tuoi prodotti al Veneziano GAS</title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php require "../components/header.php" ?>

    <div id="content">
        <?php require "../components/left_content.php" ?>


        <div id="right_content">
            <div id="title">PROPONI I TUOI PRODOTTI AL VENEZIANO GAS</div><!-- end title div -->
            <div id="article">
                <div id="article2">Siamo sempre alla ricerca di nuovi produttori, e se i tuoi prodotti sono etici e/o
                    biologici (con preferenza per i piccoli produttori locali e possibilmente a KM0) contattaci tramite
                    il form qui sotto. Ti risponderemo al più presto.
                </div>
                <br/>
                <form action="" method="post" id="produttori">

                    <div id="formTXfield">
                        <input name="Fname" type="text" required placeholder="NOME"/><br/>
                        <input name="Email" type="email" required placeholder="EMAIL"/><br/>
                        <input name="Cell" type="text" required placeholder="CELLULARE"/><br/>

                        <textarea name="tx_area" cols="31" rows="9" wrap="physical" required
                                  placeholder="INSERISCI QUI IL TUO MESSAGGIO"></textarea>
                        <div id="formButton">
                            <input name="Send" type="submit" value="INVIA  E-MAIL" id="Send"/></div>
                        <!-- end formButton div -->

                    </div><!-- end formTXfield div -->

                </form><!--end form div-->


                <div id="article_img"><img src="../images/posaditi.jpg" width="568" height="315"/><br/>image credits:
                    ambientebio.it
                </div>
            </div><!-- end article div -->

        </div><!-- end right_content div -->
    </div><!-- end content div -->

    <?php require "../components/footer.php" ?>
    <!-- end footer div -->
</div>
<!-- end wrapper div -->


</body>
</html>
