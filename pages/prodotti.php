<?php
session_start();
require_once '../connection.php';
require_once '../components/function.php';
$connection = get_connection();
$logged = is_logged();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : pagina ufficiale : PRODOTTI E PRODUTTORI</title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
    <link href="../css/expand_collapse.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">

    <?php require "../components/header.php" ?>

    <div id="content">
        <?php require "../components/left_content.php" ?>


        <div id="right_content">
            <div id="title">Prodotti del GAS</div>
            <?php
            if ($logged)
                echo '<div id="article">Puoi scaricare la lista dei produttori in formato <a href="../Documenti/2015/produttori%202015.pdf" target="_new">.pdf da qui!</a><br/><br/></div>';
            else
                echo '<div id="article">Una volta iscritto al Veneziano G.A.S. potrai visualizzare anche altre informazioni come i nomi dei responsabili dei prodotti, come ordinare, modalità e punti di consegna.<br/></div>';
            ?>

            <?php
            $sql = 'SELECT * FROM CategoriaProdotto WHERE Id != 1';
            foreach ($connection->query($sql) as $categoria) {
                echo '<div id="title">' . $categoria['NomeCategoria'] . '</div>';
                echo '<div id="article">';
                $sql = 'SELECT * FROM Produttore WHERE Categoria =' . $categoria['ID'];
                foreach ($connection->query($sql) as $produttore) {
                    $label_id = 'header' . $produttore['Id'] . $categoria['ID'];
                    echo '<div id="expand"><input class="toggle-box" id="' . $label_id . '" type="checkbox" ><label for="' . $label_id . '">';
                    if ($produttore['Immagine']) {
                        echo '<img src="../' . $produttore['Immagine'] . '" width="55" height="40"/>';
                    }
                    echo $produttore['Name'] . ' - ' . $produttore['Prodotto'] . '</label><div><div id="expand_plus">';

                    if ($logged) {
                        $sql = 'SELECT Persona FROM Referente WHERE Produttore="' . $produttore['Id'] . '"';
                        foreach ($connection->query($sql) as $codice) {
                            $sql = 'SELECT * FROM Persona WHERE Id="' . $codice['Persona'] . '"';
                            foreach ($connection->query($sql) as $referente) {
                                echo $referente['Persona'];
                                echo '<p>Referente</p>' . $referente['Nome'] . ' - ' . $referente['Email'] . ' - ' . $referente['Cellulare'] . '<br/>';
                            }
                        }
                        echo '<p>Dove</p>' . $produttore['Dove'] . '<br/>';
                          if ($produttore['Quando']!=""){
                        echo '<p>Quando</p>' . $produttore['Quando'] . '<br/>';
                      }

                        $sql = "SELECT * FROM Documenti WHERE Id =" . $produttore['Listino'];
                        $url_listino = null;
                        $values = $connection->query($sql);
                        if ($values) {
                            foreach ($values as $listino)
                                $url_listino = $listino['Url'];
                            echo '<p><a href="' . $url_listino . '">Presentazione del produttore</a></p><br/>';
                        }
                    }
                    if ($produttore['Certificato']!=""){
                      echo '<p>CERTIFICATO</p>' . $produttore['Certificato'] . '<br/>';
                    }
                    echo '<p>SITO WEB:</p><a href="' . $produttore['Sito'] . '" target="_new">Link</a></div></div><!-- end expand_plus div --> </div><!-- end expand div -->';
                }
                echo '</div>';
            }
            ?>

        </div><!-- end right_content div -->
    </div><!-- end content div -->
    <?php require "../components/footer.php" ?>
</div>
<!-- end wrapper div -->


</body>
</html>
