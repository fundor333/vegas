<?php session_start(); ?>
<?php
require '../components/function.php';
require '../connection.php';
require_once '../components/class-phpass.php';

$User = $_SESSION["username"];
$con = get_connection();

$result = $con->query("select * from Persona where Email = '$User'");

$row = NULL;
foreach ($result as $elem) {
    $row = $elem;
}
if ($row) {
    $user["Nome"] = $row['Nome'];
    $user["Indirizzo"] = $row['Indirizzo'];
    $user["Email"] = $row['Email'];
    $user["Telefono"] = $row['Telefono'];
    $user["Cellulare"] = $row['Cellulare'];
}

if (isset($_POST['Update'])) {

    $unome = $_POST['Nome'];
    $uindirizzo = $_POST['Indirizzo'];
    $uemail = $_POST['Email'];
    $utelefono = $_POST['Telefono'];
    $ucellulare = $_POST['Cellulare'];
    $sql = "UPDATE Persona SET Nome=$unome, Indirizzo=$uindirizzo, Email=$uemail, Telefono=$utelefono, Cellulare=$ucellulare WHERE Email = $User";


    $sql = $con->query($sql);
    $_SESSION['username'] = $uemail;
    header('Location: aggiornamento_ok.php');
}

if (isset($_POST['UpdatePassword'])) {
    $manager_pw = new PasswordHash(8, TRUE);
    $password = $manager_pw->HashPassword($_POST['Password']);
    $sql = "UPDATE Persona SET `Password` = '$password' WHERE Email =  '$User'";

    $sql = $con->query($sql);
}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : pagina ufficiale : Aggiorna il tuo profilo</title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php require '../components/header.php' ?>
    <div id="content">
        <?php require '../components/left_content.php' ?>
        <div id="right_content">
            <div id="title">AGGIORNA I TUOI DATI</div>
            <!-- end title div -->
            <div id="article">
                <div id="article2">
                    Compila il modulo che trovi qua sotto per aggiornare o modificare il tuo profilo, i tuoi dati
                    verrano aggiornati automaticamente. Se invece hai dimenticato la password o vuoi essere cancellato
                    inviaci una e-mail tramite CONTATTI.<br/><br/>
                    <form action="" method="post" name="update_form">
                        <div id="formTXfield">
                            <label>Nome:<br/>
                                <input name="Nome" type="text" value="<?php echo $user["Nome"]; ?>" required/>
                            </label>
                        </div>
                        <!-- end formTXfield div -->
                        <div id="formTXfield">
                            <label>
                                Indirizzo:<br/>
                                <input name="Indirizzo" type="text" required value="<?php echo $user["Indirizzo"]; ?>"/>
                            </label>
                        </div>
                        <!-- end formTXfield div -->
                        <div id="formTXfield">
                            <label>
                                Email:<br/>
                                <input name="Email" type="email" required value="<?php echo $user["Email"]; ?>"/>
                            </label>
                        </div>
                        <!-- end formTXfield div -->
                        <div id="formTXfield">
                            <label>
                                Telefono Fisso:<br/>
                                <input name="Telefono" type="text" value="<?php echo $user["Telefono"];
                                ?>"/>
                            </label>
                        </div><!-- end formTXfield div -->
                        <div id="formTXfield">
                            <label>
                                Telefono Cellulare:<br/>
                                <input name="Cellulare" type="text" value="<?php echo $user["Cellulare"]; ?>"/>
                            </label>
                        </div><!-- end formTXfield div -->
                        <div id="formButton">
                            <input name="Update" type="submit" value="AGGIORNA DATI"/></div><!-- end formButton div -->
                    </form><!-- end Update_form-->
                    <hr>
                    Vuoi cambiare la tua password?
                    <form action="" method="post" name="password_form">
                        <div id="formTXField">
                            <label>
                                Nuova password:<br/>
                                <input name="Password" type="password" value="password"/>
                            </label>
                        </div>
                        <div id="formButton">
                            <input name="UpdatePassword" type="submit" value="Aggiorna la Password"/>
                        </div>
                    </form>
                </div>
                <br/>
            </div><!-- end article div -->

        </div><!-- end right_content div -->
    </div><!-- end content div -->

    <?php require '../components/footer.php' ?>
    <!-- end footer div -->
</div>
<!-- end wrapper div -->


</body>
</html>
