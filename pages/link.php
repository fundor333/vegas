<?php session_start(); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : pagina ufficiale : Link verso siti esterni</title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>
<body>

<div id="wrapper">
    <?php require "../components/header.php" ?>

    <div id="content">
        <?php require "../components/left_content.php" ?>

        <div id="right_content">
            <div id="title">LINK</div><!-- end title div -->
            <div id="article">
                <?php
                require_once "../connection.php";
                $connection = get_connection();
                $sql = "SELECT * FROM Link";
                foreach ($connection->query($sql) as $codice) {
                    echo '<div id="link_box">';
                    echo '<a href="' . $codice['Site'] . '" target="_new">';
                    echo '<img src="http://' . $_SERVER['HTTP_HOST'] . $codice['Image'] . '" width="215" height="215" alt="' . $codice['Testo'] . ' longdesc="' . $codice['Site'] . '"/>';
                    echo '</a></div><!-- end link_box div -->';
                }
                ?>

            </div><!-- end article div -->


        </div><!-- end right_content div -->
    </div><!-- end content div -->

    <?php require "../components/footer.php" ?>
</div>
<!-- end wrapper div -->


</body>
</html>
