<?php session_start(); ?>

<?php require '../components/function.php'; ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : pagina ufficiale : modulo contati</title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <div id="header">
        <p> VENEZIANO G.A.S. - GRUPPO DI ACQUISTO SOLIDALE</p>
        <div id="logo"><img src="../images/logo_veGas.png" width="250" height="164" alt="Veneziano GAS logo"
                            longdesc="http://www.venezianogas.net"/></div>


        <div id="nmenu">
            <div id="navMenu_logIn">
                <ul>
                    <li><a href="benvenuto.php">HOME</a></li> <!-- end main LI -->
                </ul><!-- end main UL -->


                <ul>
                    <li><a href="ruoli.php">RUOLI</a>
                        <ul>
                            <li><a href="prodotti.php">PRODOTTI</a></li><!-- end inner LI-->
                        </ul><!-- end inner UL -->

                    </li><!-- end main LI -->
                </ul><!-- end main UL -->

                <ul>
                    <li><a href="documenti.php">DOCUMENTI</a></li>
                    <!-- end main LI -->
                </ul><!-- end main UL -->

                <ul>
                    <li><a href="istruzioni.php">ISTRUZIONI</a>

                    </li><!-- end main LI -->
                </ul><!-- end main UL -->
                <ul>
                    <li><a href="contattiGAS.php">CONTATTI</a>

                    </li><!-- end main LI -->
                </ul><!-- end main UL -->
                <br class="clearFloat"/>
            </div> <!-- end navMenu div -->
        </div><!-- end nmenu div -->
    </div> <!-- end header div -->

    <div id="content">
        <?php require "../components/left_content.php"; ?>

        <div id="right_content">
            <div id="title">MODULO CONTATTI</div><!-- end title div -->
            <div id="article">
                Compila il modulo ti risponderemo al più presto. Ti ricordiamo che per poterti contattare tutti i campi
                da compilare sono obbligatori.<br/><br/>
                <form action="" method="post" id="contatti">
                    <div id="formTXfield">
                        <input name="Fname" type="text" required placeholder="NOME"/><br/>
                        <input name="Email" type="email" required placeholder="EMAIL"/><br/>
                        <textarea name="tx_area" cols="31" rows="9" wrap="physical" required
                                  placeholder="INSERISCI QUI IL TUO MESSAGGIO"></textarea>
                        <div id="formButton">
                            <input name="Send" type="submit" value="INVIA  E-MAIL" id="Send"/></div>
                        <!-- end formButton div -->

                    </div><!-- end formTXfield div -->

                </form><!--end form div-->
            </div><!-- end article div -->

        </div><!-- end right_content div -->
    </div><!-- end content div -->

    <?php
    require '../components/footer.php';
    ?> <!-- end footer div -->
</div>
<!-- end wrapper div -->


</body>
</html>
