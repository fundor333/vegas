<?php session_start();
require '../components/function.php';
logged_needed();
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : pagina ufficiale : Upload</title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php require "../components/header.php"; ?>

    <div id="content">
        <?php require "../components/left_content.php"; ?>
        <div id="right_content">
            <div id="title">BENVENUTO NELL'AREA RISERVATA</div>
            <!-- end title div -->
            <div id="article">
                <form method="post" action="../uploader_verbale.php" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="upload"/>
                    <p>
                        <label>Carica il tuo file:</label>
                        <br/>
                        <input type="file" name="user_file"/>
                    </p>
                    <br/>
                    <p>
                        <?php
                        $connection = get_connection();
                        $sql = 'SELECT * FROM Gruppi';
                        echo '<label>Seleziona il gruppo o il sottogruppo di cui carichi il verbale</label><br/>';
                        foreach ($connection->query($sql) as $gruppo) {
                            echo '  <input type="radio" name="gruppo" value="' . $gruppo['Id'] . '">' . $gruppo['Name'] . '<br>';
                        }
                        ?>
                    </p>
                    <br/>
                    <p>
                        <label>Inserisci la data del verbale (formato aa-mm-gg)<br/>
                            <input type="date" name="mydatetime">
                        </label>
                    </p>
                    <br/>
                    <input type="submit" value="Carica verbale"/>
                </form>
            </div><!-- end article div -->

        </div><!-- end right_content div -->
    </div><!-- end content div -->

    <?php
    require '../components/footer.php';
    ?> <!-- end footer div -->
</div>
<!-- end wrapper div -->


</body>
</html>
