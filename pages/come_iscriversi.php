<?php session_start(); ?>
<?php require '../components/function.php'; ?>
<?php

if (isset($_POST['LogIn'])) {

    $Email = $_POST['Email'];
    $PW = $_POST['Password'];

    $result = $con->query("select * from gassisti where Email='$Email' AND Password='$PW'");

    $row = $result->fetch_array(MYSQLI_BOTH);

    session_start();

    $_SESSION["ID"] = $row['ID'];

    header('Location: account.php');
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : pagina ufficiale : Come Iscriversi</title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php require "../components/header.php" ?>
    <div id="content">
        <?php
        require "../components/left_content.php"
        ?>

        <div id="right_content">
            <div id="title">REGISTRAZIONE: COME ISCRIVERSI</div><!-- end title div -->
            <div id="article">
                <div id="article2">La maggior parte delle informazioni relative a prodotti e incontri si scambiano via
                    e-mail attraverso una mailing list. Tutti i prodotti si ordinano direttamente via internet dal
                    portale dell'economia solidale o in alcuni casi tramite e-mail. Una volta iscritto potrai creare la
                    tua password e accedere alle pagine riservate del Veneziano G.A.S. ed Economia Solidale. Riceverai
                    maggiori istruzioni in seguito, tramite il KIT DI BENVENUTO scaricabile in fomato PDF dal nostro
                    sito e tramite il gruppo acoglienza che rispondera alle tue domande. Inoltre ti arriveranno notifice
                    via e-mail su quando apre un ordine, quando chiude ed il giorno previsto per la consegna. Potrai
                    visualizzare tramite acesso al portale gli ordini che hai inviato. Inoltre riceverai notizie sulle
                    varie inziative che il Veneziano G.A.S. promuve nel territorio.<br/>
                    <br/>
                    Partecipare al VenezianoGas è semplice: invia un messaggio al GRUPPO ACCOGLIENZA tramite il form
                    contatto e riceverai il calendario dei prossimi incontri e tutte le indicazioni necessarie. La quota
                    di adesione al gruppo è di Euro 25,00. Il rinnovo dell&rsquo;associazione è annuale, e prevede un
                    contributo di Euro 10,00. L&rsquo;adesione al VenezianoGas è individuale.
                </div>
                <br/>
            </div><!-- end article div -->

            <div id="title">RICHIESTE D'ISCRIZIONE</div><!-- end title div -->
            <div id="article">
                Compila il modulo ti risponderemo al più presto. Ti ricordiamo che per poterti contattare tutti i campi
                da compilare sono obbligatori.
                <a href="https://docs.google.com/forms/d/e/1FAIpQLSevdml25yDB52UAdzzRjdXYONTwhGHbMFqCy-MOkpfmgrIl0A/viewform?c=0&w=1">Clicca
                    qui per andare al modulo</a><br/><br/>

                <!--
                <form action="" method="post" id="contatti">

                    <div id="formTXfield">
                        <input name="Fname" type="text" required placeholder="NOME"/><br/>
                        <input name="Email" type="email" required placeholder="EMAIL"/><br/>
                        <input name="Cell" type="text" required placeholder="CELLULARE"/><br/>

                        <select name="Gruppo2" size="1" required id="Gruppo">
                            <option value="000" selected>SOTTOGRUPPO</option>
                            <option value="Cannareggio Alto">Cannareggio Alto</option>
                            <option value="Cannaregio Basso">Cannaregio Basso</option>
                            <option value="Castello San Marco">Castello / San Marco</option>
                            <option value="Giudecca">Giudecca</option>
                            <option value="S Croce Polo Dorsoduro">S. Croce / Dorsoduro / S. Polo</option>
                            <option value="Lido">Lido</option>
                        </select>

                        <textarea name="tx_area" cols="31" rows="9" wrap="physical" required
                                  placeholder="INSERISCI QUI IL TUO MESSAGGIO"></textarea>
                        <div id="formButton">
                            <input name="Send" type="submit" value="INVIA  E-MAIL" id="Send"/></div>

                    </div>

                </form>
                -->
            </div>

            <div id="title">ESALOGO</div><!-- end title div -->
            <div id="article">
                <div id="article2">1. Partecipa regolarmente alle riunioni.<br/>
                    2. Le riunioni plenarie e di sottogruppo hanno finalità diverse: partecipa ad entrambe.<br/>
                    3. Non usare l’indirizzo della mailing list (messaggi che arrivano a tutti) per comunicazioni
                    dirette a una singola persona (per esempio al capo gruppo) o per fare gli ordini.<br/>
                    4. Quando arrivano i prodotti a casa di qualcuno, vai a ritirarli tempestivamente.<br/>
                    5. Senti la responsabilità di assumere impegni o cariche utili al GAS (es. ricevere merce a casa,
                    diventare responsabile di prodotto ecc.).<br/>
                    6. Proponi incontri di formazione e, già che ci sei, organizzali.
                </div>
                <br/>
            </div><!-- end article div -->
            <div id="title">STATUTO</div>
            <!-- end title div -->
            <div id="article">
                <div id="article2">Veneziano G.A.S. – Gruppo di Acquisto Solidale<br/>
                    <br/>
                    Art.1 – Oggetto e scopo<br/>
                    Il comitato non ha scopo di lucro e persegue esclusivamente finalità di solidarietà sociale. Agisce
                    per promuovere l&rsquo;esercizio di nuove forme di responsabilità etica e politica attraverso la
                    modificazione degli stili e della qualità del consumo. A tal fine il comitato così individua il
                    programma della propria azione:<br/>
                    sostenere il consumo e la diffusione di prodotti biologici, naturali, eco-compatibili;<br/>
                    sviluppare rapporti diretti con i produttori biologici, privilegiando i piccoli produttori e
                    garantendo un&rsquo;equa remunerazione del lavoro;<br/>
                    favorire la solidarietà tra i componenti del comitato.<br/>
                    Gli strumenti utilizzati per perseguire le finalità programmatiche sopra indicate sono:<br/>
                    l&rsquo;acquisto collettivo dei prodotti sempre escluso qualsiasi fine di lucro;<br/>
                    lo svolgimento di attività di assistenza, formazione e informazione nel campo alimentare biologico e
                    nei settori ad esso collegati (modalità di produzione e di distribuzione, &ldquo;ricette&rdquo; per
                    l&rsquo;uso, impatto ambientale, ecc.);<br/>
                    la promozione dei prodotti eco – compatibili e delle loro tecniche di produzione ed utilizzo;<br/>
                    il compimento di tutte le operazioni commerciali, finanziarie, immobiliari atte al raggiungimento
                    dello scopo sociale.<br/>
                    Il comitato non può svolgere attività diverse da quelle sopra indicate ad eccezione di quelle ad
                    esse connesse e di quelle accessorie a quelle statutarie, in quanto integrative delle stesse.<br/>
                    <br/>
                    Art. 2 – Fondo Comune<br/>
                    Il fondo comune del comitato è formato dai contributi inizialmente versati dai promotori e dai
                    contributi successivamente versati dai componenti che si aggiungeranno al comitato nonché dalle
                    oblazioni raccolte da eventuali sottoscrittori del programma finalizzate al raggiungimento dello
                    scopo dichiarato, salvo l&rsquo;accettazione dei componenti del comitato.<br/>
                    Il fondo di dotazione del comitato è costituito dai versamenti effettuati dagli aderenti.<br/>
                    La quota d&rsquo;adesione è stabilita in 25,00 €<br/>
                    <br/>
                    Art. 3 – Adesione<br/>
                    L&rsquo;adesione al comitato è componentsera ed aperta a tutti.<br/>
                    <br/>
                    Art. 4 – Organizzazione del Comitato<br/>
                    La gestione del Comitato è praticata collegialmente da tutti gli aderenti tramite riunioni
                    periodiche.<br/>
                    Gli aderenti:<br/>
                    stabiliscono luoghi, modi e tempi delle proprie riunioni;<br/>
                    indirizzano l&rsquo;attività del comitato;<br/>
                    stabiliscono le iniziative per la conservazione e la disposizione dei fondi in conformità allo scopo
                    del comitato;<br/>
                    decidono le modifiche al presente Statuto nonché i modi per renderle effettive;<br/>
                    dispongono lo scioglimento e la liquidazione del comitato e la devoluzione del fondo comune.<br/>
                    La rappresentanza del comitato di fronte a terzi o in giudizio è affidata a quegli aderenti che
                    hanno agito in nome e per conto del comitato.<br/>
                    Il presente Statuto può essere modificato nel corso di una qualsiasi riunione degli aderenti e su
                    proposta di qualsiasi aderente. La proposta di discutere della modifica deve ricevere il voto
                    favorevole della maggioranza dei presenti; la modifica si intenderà accolta quando riceverà il voto
                    della maggioranza assoluta degli aderenti.
                </div>
                <br/>

            </div><!-- end right_content div -->
        </div><!-- end content div -->
        <?php require "../components/footer.php" ?>
    </div>
</div>
<!-- end wrapper div -->


</body>
</html>
