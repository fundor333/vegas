<?php session_start();
require '../connection.php';
require_once '../components/class-phpass.php';
require_once '../components/mail.php';
require_once '../components/function.php';

$connection = get_connection();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : pagina ufficiale : Aggiorna il tuo profilo</title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php require '../components/header.php' ?>
    <div id="content">
        <?php require '../components/left_content.php' ?>
        <div id="right_content">
            <div id="title">Recupero Password</div>
            <!-- end title div -->
            <div id="article">
                <div id="article2">
                    <?php
                    //funzione che crea una password random

                    if(isset($_GET['hash'])){

                        $hash=$_GET['hash'];

                        $id=substr($hash, 32);



                        //controllo che i valori dell’hash corrispondano ai valori salvati nel database
                        $sql ="SELECT * FROM Persona WHERE `Password`  ='".$_GET['hash']."'";
                        $user = null;
                        foreach ($connection->query($sql) as $row) {
                            $user = $row;
                        }

                        if($user){

                            $row=$user;

                            $email=$row['Email'];
                            $password=random(8); //nuova password di 8 caratteri
                            send_new_password($email,$password);
                            //salvo la nuova password al posto della vecchia (in md5)
                            $sql = "UPDATE Persona SET `Password` = '$password_new' WHERE `Password` =  '".$_GET['hash']."'";

                            $sql = $connection->query($sql);
                        }

                    } //if(isset($_GET['hash']))
                    ?>
                </div>
                <br/>
            </div><!-- end article div -->

        </div><!-- end right_content div -->
    </div><!-- end content div -->

    <?php require '../components/footer.php' ?>
    <!-- end footer div -->
</div>
<!-- end wrapper div -->


</body>
</html>
