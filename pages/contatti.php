<?php session_start(); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : pagina ufficiale : modulo contati</title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php require "../components/header.php" ?>

    <div id="content">
        <?php require "../components/left_content.php" ?>

        <div id="right_content">
            <div id="title">MODULO CONTATTI</div><!-- end title div -->
            <div id="article">
                Compila il modulo ti risponderemo al più presto. Ti ricordiamo che per poterti contattare tutti i campi
                da compilare sono obbligatori.<br/><br/>
                <form action="" method="post" id="contatti">
                    <div id="formTXfield">
                        <input name="Fname" type="text" required placeholder="NOME"/><br/>
                        <input name="Email" type="email" required placeholder="EMAIL"/><br/>
                        <textarea name="tx_area" cols="31" rows="9" wrap="physical" required
                                  placeholder="INSERISCI QUI IL TUO MESSAGGIO"></textarea>
                        <div id="formButton">
                            <input name="Send" type="submit" value="INVIA  E-MAIL" id="Send"/></div>
                        <!-- end formButton div -->

                    </div><!-- end formTXfield div -->

                </form><!--end form div-->

            </div><!-- end article div -->

        </div><!-- end right_content div -->
    </div><!-- end content div -->
    <?php require "../components/footer.php" ?>

</div>
<!-- end wrapper div -->


</body>
</html>
