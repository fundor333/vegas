<?php session_start();
require '../components/function.php';
logged_needed();
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : pagina ufficiale : Gasista</title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php require "../components/header.php"; ?>

    <div id="content">
        <?php require "../components/left_content.php"; ?>
        <div id="right_content">
            <div id="title">BENVENUTO NELL'AREA RISERVATA</div>
            <!-- end title div -->
            <div id="article">
                <?php

                echo '<div id="article">';

                $connection = get_connection();
                echo '<h1>Sottogruppi e altri incarichi</h1>';

                $sql = "SELECT * FROM Ruoli";
                foreach ($connection->query($sql) as $referente_row) {
                    $id = $referente_row['Persona'];
                    $nome_produttore = $referente_row['Nome'];

                    $sql = 'SELECT * FROM Persona WHERE Id =' . $id;
                    $queried = ($connection->query($sql));
                    $persona = $queried->fetch();
                    echo '<h2>' . $persona['Nome'] . '</h2>';
                    echo '<p>' . $nome_produttore . '</p>';
                    echo '<p>' . $persona['Indirizzo'] . '</p>';
                    echo '<p>' . $persona['Email'] . '</p>';
                    echo '<p>' . $persona['Telefono'] . '</p>';
                    echo '<p>' . $persona['Cellulare'] . '</p>';
                }

                $sql = "SELECT * FROM CapoGruppo";
                foreach ($connection->query($sql) as $referente_row) {
                    $id = $referente_row['Persona'];
                    $nome_produttore = $referente_row['Gruppo'];

                    $sql = 'SELECT Name FROM Gruppi WHERE Id =' . $nome_produttore;
                    $queried = ($connection->query($sql));
                    $nome_produttore = $queried->fetch()['Name'];

                    $sql = 'SELECT * FROM Persona WHERE Id =' . $id;
                    $queried = ($connection->query($sql));
                    $persona = $queried->fetch();
                    echo '<h2>' . $persona['Nome'] . '</h2>';
                    echo '<p>' . $nome_produttore . '</p>';
                    echo '<p>' . $persona['Indirizzo'] . '</p>';
                    echo '<p>' . $persona['Email'] . '</p>';
                    echo '<p>' . $persona['Telefono'] . '</p>';
                    echo '<p>' . $persona['Cellulare'] . '</p>';
                }


                $connection = get_connection();
                echo '<h1>Referenti prodotti</h1>';
                $sql = "SELECT * FROM Referente";
                foreach ($connection->query($sql) as $referente_row) {
                    $id = $referente_row['Persona'];
                    $produttore = $referente_row['Produttore'];

                    $sql = 'SELECT Name FROM Produttore WHERE Id =' . $produttore;
                    $queried = ($connection->query($sql));
                    $feched = $queried->fetch();
                    $nome_produttore = $feched['Name'];

                    $sql = 'SELECT * FROM Persona WHERE Id =' . $id;
                    $queried = ($connection->query($sql));
                    $persona = $queried->fetch();
                    echo '<h2>' . $persona['Nome'] . '</h2>';
                    echo '<p>' . $nome_produttore . '</p>';
                    echo '<p>' . $persona['Indirizzo'] . '</p>';
                    echo '<p>' . $persona['Email'] . '</p>';
                    echo '<p>' . $persona['Telefono'] . '</p>';
                    echo '<p>' . $persona['Cellulare'] . '</p>';
                }

                echo '</div>';
                ?>
            </div><!-- end article div -->

        </div><!-- end right_content div -->
    </div><!-- end content div -->

    <?php
    require '../components/footer.php';
    ?> <!-- end footer div -->
</div>
<!-- end wrapper div -->


</body>
</html>
