<?php session_start(); ?>
<?php
require '../components/function.php';
require '../connection.php';
logged_needed();
$sql = 'SELECT * FROM Gruppi WHERE Id=' . $_GET['id'];
$connection = get_connection();
$gruppo = null;
foreach ($connection->query($sql) as $row) {
    $gruppo = $row;
}
$id = $gruppo['Id'];
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : pagina ufficiale : Verbali gruppo <?php echo $gruppo['Name']; ?></title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php require '../components/header.php' ?>

    <div id="content">
        <?php require '../components/left_content.php' ?>
        <div id="right_content">
            <div id="title">Verbali gruppo <?php echo $gruppo['Name']; ?></div>
            <!-- end title div -->
            <div id="article">
                <table width="100%" border="0">
                    <?php
                    $query = "SELECT * FROM Documenti WHERE Id IN (SELECT Documento FROM DocumentoXGruppo Where Gruppo=$id) ORDER BY Data;";
                    foreach ($connection->query($query) as $row) {
                        echo '<tr><td width="25%">' . $row['Data'] . '</td>';
                        echo '<td width="25%"><a href="' . $row['Url'] . '">' . $row['Name'] . '</a></td</tr>';
                    }
                    ?>
                    <tr>
                    </tr>
                </table>
            </div><!-- end article div -->

        </div><!-- end right_content div -->
    </div><!-- end content div -->

    <?php require '../components/footer.php' ?>
    <!-- end footer div -->
</div>
<!-- end wrapper div -->


</body>
</html>
