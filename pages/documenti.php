<?php session_start();
require '../components/function.php';
logged_needed();

require '../connection.php';
$connection = get_connection();
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : pagina ufficiale : Proponi i tuoi prodotti al Veneziano GAS</title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php
    require '../components/header.php';
    ?>
    <div id="content">
        <?php require "../components/left_content.php"; ?>
        <div id="right_content">
            <div id="title">DOCUMENTI SCARICABILI DA SITO DI VENEZIANO GAS</div>
            <!-- end title div -->
            <div id="article">
                Qui trovi l'elenco di tutti i documenti che puoi scaricare dal nostro sito divisi per categorie ed in
                ordine cronologico (dal più recente a quello meno recente).<br/><br/>Seleziona dall'elenco per essere
                reindirizzato alla categoria desiderata.<br/><br/>
                <p><a href="upload_verbali.php">Qui</a> puoi caricare i verbali.</p>
                <br/>
                <hr/><table width="100%" border="0">
                    <tr>
                        <td width="34%">VERBALI</td>
                    </tr>
                    <?php
                    $sql = "SELECT * FROM Gruppi WHERE Id IN (SELECT Gruppo FROM DocumentoXGruppo GROUP BY Gruppo)";
                    foreach ($connection->query($sql) as $gruppo) {
                        echo '<tr><td width="34%"></td>';
                        echo '<td width="66%"><a href="verbali.php?id=' . $gruppo['Id'] . '">Gruppo ' . $gruppo['Name'] . '</a></td>';
                        echo '</tr>';
                    }
                    ?>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>DOCUMENTI</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="34%"></td>
                        ';
                        <td width="66%"><a
                                    href="http://www.venezianogas.net/uploads/accoglienza/Calendario_acquisti.pdf">
                                Calendario Acquisti
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td width="34%"></td>
                        ';
                        <td width="66%"><a
                                    href="http://www.venezianogas.net/uploads/accoglienza/vita del Venezianogas.pdf">
                                Vita del Veneziano Gas
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td width="34%"></td>
                        ';
                        <td width="66%"><a
                                    href="http://www.venezianogas.net/uploads/accoglienza/Statuto Venezianogas def 10 04 2017.pdf">
                                Statuto del Veneziano Gas
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td width="34%"></td>
                        ';
                        <td width="66%"><a
                                    href="http://www.venezianogas.net/uploads/accoglienza/scaletta argomenti per nuovi gasisti.pdf">
                                Calendario Acquisti
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td width="34%"></td>
                        ';
                        <td width="66%"><a
                                    href="http://www.venezianogas.net/uploads/accoglienza/Modalità di acquisto.pdf">
                                Modalità di Acquisto
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><a href="../pdf/pdf_produttori.php">Elenco Produttori</a></td>
                    </tr>
                    <?php
                    $sql = "SELECT * FROM CategoriaProdotto WHERE ID IN (SELECT Categoria FROM Produttore GROUP BY Categoria)";
                    foreach ($connection->query($sql) as $gruppo) {
                        echo '<tr><td width="34%"></td>';
                        echo '<td width="66%"><a href="../pdf/pdf_produttori.php?id=' . $gruppo['ID'] . '">' . $gruppo['NomeCategoria'] . '</a></td>';
                        echo '</tr>';
                    }
                    ?>
                    <tr>
                        <td>Rubrica</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><a href="../pdf/pdf_contatti.php">Rubrica Completa</a></td>
                    </tr>
                    <?php
                    $connection = get_connection();
                    $sql = "SELECT * FROM Gruppi WHERE Id IN (SELECT Sottogruppo FROM Persona GROUP BY Sottogruppo)";
                    foreach ($connection->query($sql) as $gruppo) {
                        echo '<tr><td width="34%"></td>';
                        echo '<td width="66%"><a href="../pdf/pdf_contatti.php?id=' . $gruppo['Id'] . '">Rubrica Sottogruppo ' . $gruppo['Name'] . '</a></td>';
                        echo '</tr>';
                    }
                    ?>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>NEWSLETTER</td>
                        <td>EL FONTEGO</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>...</td>
                    </tr>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>ALTRO</td>
                        <td>KIT DI BENVENUTO</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>ISTRUZIONI CARICAMENTO FILE</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>ALTRE RIUNIONI ED INCONTRI</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>


            </div><!-- end article div -->

        </div><!-- end right_content div -->
    </div><!-- end content div -->

    <?php
    require '../components/footer.php'
    ?>
</div>
<!-- end wrapper div -->


</body>
</html>
