<?php session_start();
require '../components/function.php';
logged_needed();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : pagina ufficiale :Benvenuto nell'area riservata</title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php
    require '../components/header.php';
    ?>

    <div id="content">
        <?php require "../components/left_content.php"; ?>
        <div id="right_content">
            <div id="title">BENVENUTO NELL'AREA RISERVATA <?php echo $_SESSION["Fname"]; ?></div>
            <!-- end title div -->
            <div id="article">

                Ti sei autenticato con sucesso!!! <br/>
                Qui troverai tutte le informazioni necessarie, e gli eventi importanti. Se hai un evento importante da
                segnalare legate al Veneziano Gas (come convocazione sottogruppi e plenarie, scadenze e aperture ordini,
                o consegne ordini, puoi aggiungerli anche tu dal link "Add Event". <br/><br/><br/>
                <iframe
                        src="https://calendar.google.com/calendar/embed?src=s9tq6fg6uruvqe9cjia7pt89jg%40group.calendar.google.com&ctz=Europe/Rome"
                        style="border: 0" width="720" height="600" frameborder="0" scrolling="no"></iframe>
                Per rendere il calendario chiaro e funzionale vi preghiamo di non aggiungere eventi ed attività esterne
                al Veneziano GAS

            </div><!-- end article div -->

        </div><!-- end right_content div -->
    </div><!-- end content div -->

    <?php
    require '../components/footer.php';
    ?> <!-- end footer div -->
</div>
<!-- end wrapper div -->


</body>
</html>
