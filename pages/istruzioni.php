<?php session_start();
require '../components/function.php';
logged_needed();
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : pagina ufficiale : Proponi i tuoi prodotti al Veneziano GAS</title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php require "../components/header.php" ?>
    <div id="content">
        <?php require "../components/left_content.php"; ?>
        <div id="right_content">
            <div id="title">BENVENUTO NELL'AREA RISERVATA</div>
            <!-- end title div -->
            <div id="article">
                <div id="article2"></div>
                <br/>
            </div><!-- end article div -->

        </div><!-- end right_content div -->
    </div><!-- end content div -->

    <?php require '../components/footer.php'
    ?> <!-- end footer div -->
</div>
<!-- end wrapper div -->


</body>
</html>
