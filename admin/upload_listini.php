<?php
session_start();
require_once '../components/function.php';
require_once '../connection.php';
if ($_SESSION['admin'] == false) {
    header('Location: ' . "http://$_SERVER[HTTP_HOST]" . '/pages/benvenuto.php');
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : pagina ufficiale : Upload listini</title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php require "../components/header.php"; ?>

    <div id="content">
        <?php require "../components/left_content.php"; ?>
        <div id="right_content">
            <div id="title">BENVENUTO NELL'AREA RISERVATA</div>
            <!-- end title div -->
            <div id="article">
                <form method="post" action="../uploader_listino.php" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="upload"/>
                    <p>
                        <label>Aggiorna il documento:</label>
                        <br/>
                        <input type="file" name="user_file"/>
                    </p>
                    <br/>
                    <p>

                        <?php
                        $connection = get_connection();
                        //TODO FIX UPLODER
                        $sql = 'SELECT * FROM Produttore ORDER BY Name';
                        echo '<label>Seleziona il produttore</label><br/>';
                        echo '<select name="produttore" >';
                        foreach ($connection->query($sql) as $produttore) {
                            echo '  <option value=" ' . $produttore['Id'] . '">' . $produttore['Name'] . '</option><br>';
                        }
                        echo '</select>';
                        ?>
                    </p>
                    <br/>
                    <input type="submit" value="Carica documento"/>
                </form>
            </div><!-- end article div -->

        </div><!-- end right_content div -->
    </div><!-- end content div -->

    <?php
    require '../components/footer.php';
    ?> <!-- end footer div -->
</div>
<!-- end wrapper div -->


</body>
</html>
