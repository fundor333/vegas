<?php
session_start();
require_once '../components/function.php';
require_once '../connection.php';
if ($_SESSION['admin'] == false) {
    header('Location: ' . "http://$_SERVER[HTTP_HOST]" . '/pages/benvenuto.php');
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : Admin</title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php require "header.php"; ?>

    <div id="content">
        <div id="title">BENVENUTO NELL'AREA RISERVATA</div>
        <!-- end title div -->
        <div id="article">
            <table>
                <tr>
                    <td><a href="admin_new_user.php">️ ➕ </a></td>
                    <td>Nuovo Utente</td>
                    <td></td>
                </tr>
                <?php
                $connection = get_connection();
                $sql = 'SELECT Nome,Email,Permessi,Id FROM Persona ORDER BY Nome';
                echo "<table";
                foreach ($connection->query($sql) as $persona) {
                    $sql = 'SELECT Label FROM Permessi WHERE Id=' . $persona['Permessi'];
                    $label_permessi = $connection->query($sql)->fetch()['Label'];
                    echo "<tr>";
                    echo '<td><a href="admin_change_user.php?id=' . $persona['Id'] . '">✒️  </a></td>';
                    echo '<td>' . $persona['Nome'] . '</td>';
                    echo '<td>' . $persona['Email'] . '</td>';
                    echo '<td>' . $label_permessi . '</td>';
                    echo '<td><a href="admin_destroy_user.php?id=' . $persona['Id'] . '">🗑 ️</a></td>';
                    echo "</tr>";
                }
                echo "</table>";

                ?>
        </div><!-- end article div -->

    </div><!-- end content div -->

    <?php
    require 'footer.php';
    ?> <!-- end footer div -->
</div>
<!-- end wrapper div -->


</body>
</html>
