<?php
session_start();
require_once '../components/function.php';
require_once '../connection.php';
if ($_SESSION['admin'] == false) {
    header('Location: ' . "http://$_SERVER[HTTP_HOST]" . '/pages/benvenuto.php');
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : Admin</title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php require "header.php"; ?>

    <div id="content">
        <div id="title">BENVENUTO NELL'AREA RISERVATA</div>
        <!-- end title div -->
        <div id="article">
            <div>
                Numero Gasisti
                <?php
                $sql = " SELECT COUNT(*) AS Num FROM Persona WHERE (Permessi =1 OR Permessi =2) ";
                $connection = get_connection();
                echo $connection->query($sql)->fetch()['Num'];
                ?>
                <br/>
                Admin
                <?php
                $sql = " SELECT COUNT(*) AS Num FROM Persona WHERE (Permessi =2) ";
                $connection = get_connection();
                echo $connection->query($sql)->fetch()['Num'];
                ?>
                <br/>
                Non ancora Iscritti
                <?php
                $sql = " SELECT COUNT(*) AS Num FROM Persona WHERE Permessi =0";
                $connection = get_connection();
                echo $connection->query($sql)->fetch()['Num'];
                ?>
                <br/>
                Ex Gasisti
                <?php
                $sql = " SELECT COUNT(*) AS Num FROM Persona WHERE Permessi =4 ";
                $connection = get_connection();
                echo $connection->query($sql)->fetch()['Num'];
                ?>
                <br/>
                Senza Codice Fiscale
                <?php
                $sql = " SELECT COUNT(*) AS Num FROM Persona WHERE (Permessi =1 OR Permessi =2) AND CodiceFiscale IS Null ";
                $connection = get_connection();
                echo $connection->query($sql)->fetch()['Num'];
                ?>
            </div>


        </div><!-- end article div -->

    </div><!-- end content div -->

    <?php
    require 'footer.php';
    ?> <!-- end footer div -->
</div>
<!-- end wrapper div -->


</body>
</html>
