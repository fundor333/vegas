<?php
session_start();
require_once '../components/function.php';
require_once '../connection.php';
if ($_SESSION['admin'] == false) {
    header('Location: ' . "http://$_SERVER[HTTP_HOST]" . '/pages/benvenuto.php');
}
$con = get_connection();
$query = $con->query("SELECT * FROM Produttore WHERE Id = '" . $_GET['id'] . "'");
$row = $query->fetch();

if ($row) {

    $produttore['Id'] = $_GET['Id'];
    $produttore["Name"] = $row['Name'];
    $produttore['Prodotto'] = $row['Prodotto'];
    $produttore['Quando'] = $row['Quando'];
    $produttore['Dove'] = $row['Dove'];
    $produttore['Certificato'] = $row['Certificato'];
    $produttore['Modalita'] = $row['Modalità'];
    $produttore['Sito'] = $row['Sito'];
    $produttore['Categoria'] = $row['Categoria'];
    $produttore['Immagine'] = $row['Immagine'];

}

if (isset($_POST['Update'])) {

    $uid = $_POST["Id"];
    $unome = $_POST['Nome'];
    $uprodotto = $_POST['Prodotto'];

//TODO Reworking quando
    $uquando = array($_POST['1'], $_POST['2'], $_POST['3'], $_POST['4'], $_POST['5'], $_POST['6'], $_POST['7'], $_POST['8'], $_POST['9'], $_POST['10'], $_POST['11'], $_POST['12']);

    foreach (array_keys($_POST) as $array_key) {
        echo $array_key . " ";
    }


    $udove = $_POST['Dove'];
    $ucertificato = $_POST['Certificato'];
    $umodalita = $_POST["Modalita"];
    $usito = $_POST['Sito'];
    $ucategoria = $_POST['Categoria'];
    $uimmagine = $_POST['Immagine'];

    if ($uid == "null") {
        $sql = "INSERT INTO Produttore (Name,Prodotto,Dove,Certificato,Modalita,Sito,Categoria,Immagine) VALUES ($unome,$uprodotto,$udove,$ucertificato,$umodalita,$usito,$ucategoria,$uimmagine);";
    } else {
        $sql = "UPDATE Produttore SET Name=$unome,Prodotto=$uprodotto,Dove=$udove, Certificato = $ucertificato, Modalita=$umodalita, Sito=$usito, Categoria=$ucategoria, Immagine=$uimmagine WHERE Id = $uid";
    }

    $con->query($sql);

    $sql = "DELETE FROM MeseXProduttore WHERE Produttore =" . $uid;
    $con->query($sql);


    for ($i = 0; $i < 12; $i++) {
        if ($uquando[$i]) {
            $sql = "Insert INTO MeseXProduttore (Mese,Prodotto) VALUES ('.($i+1).','.$uid.')";
            $con->query($sql);
        }
    }

}

?>


<!DOCTYPE html PUBLIC " -//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http - equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title> VENEZIANO GAS : Admin </title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php require "header.php"; ?>

    <div id="content">
        <div id="title">BENVENUTO NELL'AREA RISERVATA</div>
        <!-- end title div -->
        <div id="article">
            <tr id="article2">
                Compila il modulo che trovi qua sotto per aggiornare o modificare il profilo del produttore.<br/><br/>
                <form action="" method="post" name="admin_change_produttore">
                    <table>
                        <tr>
                            <td>Nome:</td>
                            <td><input name="Nome" type="text" value="<?php echo $produttore["Name"]; ?>"/></td>
                        </tr>
                        <tr>
                            <td>Prodotto:</td>
                            <td>
                                <?php
                                $sql = "SELECT * FROM `CategoriaProdotto` ";

                                echo '<select name="Categoria" >';
                                foreach ($con->query($sql) as $prodotto) {
                                    echo "<option value=" . $prodotto['ID'];
                                    if (strcmp($prodotto['ID'], $produttore['Categoria']) == 0)
                                        echo ' selected="selected"';
                                    echo ">" . $prodotto['NomeCategoria'] . "</option>";
                                }
                                echo "</select>";
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Quando:</td>
                            <td>
                                <?php
                                $mesi = [];
                                $sql = "SELECT * FROM 'MesiXProduttore' WHERE Produttore=" . $produttore['Id'];

                                $result = $con->query($sql);
                                if ($result) {
                                    foreach ($result as $mese) {
                                        $mesi[] = $mese;
                                    }
                                }

                                $sql = "SELECT * FROM `Mesi` ";
                                foreach ($con->query($sql) as $mese) {
                                    echo '<input type="checkbox" name=' . $mese['Id'] . ' value=' . $mese['Id'];
                                    if (in_array($mese['Id'], $mesi))
                                        echo " checked=\"checked\" ";
                                    echo "> " . $mese['Name'] . " <br/>";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Dove consegna</td>
                            <td><input name="Dove" type="text" value="<?php echo $produttore["Dove"]; ?>"/></td>

                        </tr>
                        <tr>
                            <td>Certificazione del produttore</td>
                            <td><input name="Certificato" type="text"
                                       value="<?php echo $produttore["Certificato"]; ?>"/></td>
                        </tr>
                        <tr>
                            <td>Modalità della consegna</td>
                            <td><input name="Modalita" type="text" value="<?php echo $produttore["Modalita"]; ?>"/></td>
                        </tr>
                        <tr>
                            <td>Sito del produttore</td>
                            <td><input name="Sito" type="text" value="<?php echo $produttore["Sito"]; ?>"/></td>
                        </tr>
                        <tr>
                            <td>Prodotto</td>
                            <td><input name="Prodotto" type="text" value="<?php echo $produttore["Prodotto"]; ?>"/></td>
                        </tr>

                    </table>

                    <div id="formButton">
                        <input name="Update" type="submit" value="AGGIORNA DATI"/>
                    </div>
                </form><!-- end Update_form-->

        </div>

    </div><!-- end article div -->

</div><!-- end content div -->

<?php
require 'footer.php';
?> <!-- end footer div -->
</div>
<!-- end wrapper div -->


</body>
</html>
