<?php
if (!isset($_SESSION)) {
  session_start();
}
require_once '../components/function.php';
require_once '../components/class-phpass.php';
require_once '../components/mail.php';
require_once '../connection.php';
if ($_SESSION['admin'] == false) {
    header('Location: ' . "http://$_SERVER[HTTP_HOST]" . '/pages/benvenuto.php');
}
$con = get_connection();
$query = $con->query("SELECT * FROM Persona WHERE Id = '" . $_GET['id'] . "'");
$row = NULL;
foreach ($query as $elem) {
    $row = $elem;
}

if ($row) {
    $user["Nome"] = $row['Nome'];
    $user["Indirizzo"] = $row['Indirizzo'];
    $user["Email"] = $row['Email'];
    $user['CodiceFiscale'] = $row['CodiceFiscale'];
    $user["Telefono"] = $row['Telefono'];
    $user["Cellulare"] = $row['Cellulare'];
    $user["Permessi"] = $row['Permessi'];
    $user["Sottogruppo"] = $row['Sottogruppo'];

}

if (isset($_POST['Update'])) {
    $unome = $_POST['Nome'];
    $cf = $_POST['CodiceFiscale'];
    $uindirizzo = $_POST['Indirizzo'];
    $uemail = $_POST['Email'];
    $utelefono = $_POST['Telefono'];
    $ucellulare = $_POST['Cellulare'];
    $permessi = $_POST['Permessi'];
    $sottogruppo = $_POST['gruppo'];
    $sql = "UPDATE Persona SET Nome='$unome', Indirizzo='$uindirizzo', Email='$uemail', Telefono='$utelefono', Cellulare='$ucellulare', Permessi=$permessi, Sottogruppo =$sottogruppo, CodiceFiscale='$cf' WHERE Email = '".$user["Email"]."'";
    $sql = $con->query($sql);
    header('Location: admin_utenti.php');
}

if (isset($_POST['UpdatePassword'])) {
    $manager_pw = new PasswordHash(8, TRUE);
    $password = $manager_pw->HashPassword($_POST['Password']);
    $sql = "UPDATE Persona SET `Password` = '$password' WHERE Email =  '".$_POST['Email']."'";
    $sql = $con->query($sql);
    send_new_password($_POST['Email'],$_POST['Password']);
}


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : Admin</title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php require "header.php"; ?>

    <div id="content">
        <div id="title">BENVENUTO NELL'AREA RISERVATA</div>
        <!-- end title div -->
        <div id="article">
            <div id="article2">
                Compila il modulo che trovi qua sotto per aggiornare o modificare il tuo profilo, i tuoi dati
                verrano aggiornati automaticamente. Se invece hai dimenticato la password o vuoi essere cancellato
                inviaci una e-mail tramite CONTATTI.<br/><br/>
                <form action="" method="post" name="update_form">
                    <table>
                        <tr>
                            <td>Nome</td>
                            <td>
                              <input name="Nome" type="text" value="<?php echo $user["Nome"]; ?>" required/>
                            </td>
                        </tr>
                        <tr>
                            <td>Stato del Gasista:</td>
                            <td>
                                <?php
                                $sql = 'SELECT * FROM Permessi';
                                foreach ($con->query($sql) as $permesso) {
                                    echo '  <input type="radio" name="Permessi" value="' . $permesso['Id'] . '"';
                                    if ($user["Permessi"] == $permesso['Id']) {
                                        echo " checked=\"checked\" ";
                                    }
                                    echo '>' . $permesso['Label'] . '<br>';
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Indirizzo</td>
                            <td>
                                <input name="Indirizzo" type="text" required value="<?php echo $user["Indirizzo"]; ?>"/>
                            </td>
                        </tr>

                        <tr>
                            <td>Email:</td>
                            <td>
                                <input name="Email" type="email" required value="<?php echo $user["Email"]; ?>"/>
                            </td>
                        </tr>

                        <tr>
                            <td>Telefono Fisso:</td>
                            <td>
                                <input name="Telefono" type="text"  value="<?php echo $user["Telefono"];
                                ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Telefono Cellulare:</td>
                            <td>
                                <input name="Cellulare" type="text" value="<?php echo $user["Cellulare"]; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Sottogruppo:</td>
                            <td>
                                <?php
                                $sql = 'SELECT * FROM Gruppi WHERE sottogruppo = 1';
                                foreach ($con->query($sql) as $gruppo) {
                                    echo '  <input type="radio" name="gruppo" value="' . $gruppo['Id'] . '"';
                                    if ($user["Sottogruppo"] == $gruppo['Id']) {
                                        echo " checked=\"checked\" ";
                                    }
                                    echo '>' . $gruppo['Name'] . '<br>';
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Codice Fiscale:</td>
                            <td>
                                <input name="CodiceFiscale" type="text" value="<?php echo $user["CodiceFiscale"]; ?>"/>
                            </td>
                        </tr>
                    </table>
                    <div id="formButton">
                        <input name="Update" type="submit" value="AGGIORNA DATI"/>
                    </div>
                </form><!-- end Update_form-->

                Vuoi cambiare la sua password?
                <form action="" method="post" name="password_form">
                    <div id="formTXField">
                        <label>
                            <input name="Password" type="text" value=""/>
                        </label>
                        <label style="visibility: hidden">
                            <input name="Email" type="email" required value="<?php echo $user["Email"]; ?>"/>
                        </label>
                    </div>
                    <div id="formButton">
                        <input name="UpdatePassword" type="submit" value="Aggiorna la Password"/>
                    </div>
                </form>

            </div>

        </div><!-- end article div -->

    </div><!-- end content div -->

    <?php
    require 'footer.php';
    ?> <!-- end footer div -->
</div>
<!-- end wrapper div -->


</body>
</html>
