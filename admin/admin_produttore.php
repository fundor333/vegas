<?php
session_start();
require_once '../components/function.php';
require_once '../connection.php';
if ($_SESSION['admin'] == false) {
    header('Location: ' . "http://$_SERVER[HTTP_HOST]" . '/pages/benvenuto.php');
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VENEZIANO GAS : Admin</title>
    <link href="../css/layout_gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/menu_Gas.css" rel="stylesheet" type="text/css"/>
    <link href="../css/form_styles.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="wrapper">
    <?php require "header.php"; ?>

    <div id="content">
        <div id="title">BENVENUTO NELL'AREA RISERVATA</div>
        <!-- end title div -->
        <div id="article">
            <table>
                <tr>
                    <td><a href="admin_change_produttore.php?id='null'">️ ➕ </a></td>
                    <td>Nuovo produttore</td>
                    <td></td>
                </tr>
                <tr>
                  <td><a href="upload_listini.php">Upload</a> </td>
                  <td>Per caricare le presentazioni</td>
                  </tr>

                <?php
                $connection = get_connection();
                $sql = 'SELECT * FROM Produttore ORDER BY Name';
                foreach ($connection->query($sql) as $produttore) {
                    $sql = 'SELECT * FROM CategoriaProdotto WHERE Id=' . $produttore['Categoria'];
                    $query = $connection->query($sql);
                    $label_categoria = $query->fetch()['NomeCategoria'];
                    echo "<tr>";
                    echo '<td><a href="admin_change_produttore.php?id=' . $produttore['Id'] . '">✒️  </a></td>';

                    echo '<td>' . $produttore['Name'] . '</td>';
                    echo '<td>' . $label_categoria . '</td>';
                    echo '<td><a href="admin_destroy_produttore.php?id=' . $produttore['Id'] . '">🗑 ️</a></td>';
                    echo "</tr>";
                }
                ?>
            </table>
        </div><!-- end article div -->

    </div><!-- end content div -->

    <?php
    require 'footer.php';
    ?> <!-- end footer div -->
</div>
<!-- end wrapper div -->


</body>
</html>
